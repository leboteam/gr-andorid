package md.lebo.getreal.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var data = intent?.dataString

        if (data != null) {
            val activityIntent = Intent(
                    this,
                    LoginActivity::class.java
            )

            if (data != null && data.contains("https://getreal.fr/register/confirm/")) {
                data = data.replace("https://getreal.fr/register/confirm/", "")
                activityIntent.putExtra("TOKEN", data)
                startActivity(activityIntent)
            } else if (data != null && data.contains("https://getreal.fr/my-account/my-profile/confirm-email-update/")) {
                data = data.replace("https://getreal.fr/my-account/my-profile/confirm-email-update/", "")
                val values = data.split("?target=")
                Log.e("SPLASH", values.toString())
                activityIntent.putExtra("TOKEN", values[0])
                activityIntent.putExtra("TARGET", values[1])
                startActivity(activityIntent)
            }

            if (data != null && data.contains("https://getreal.stage.bixel.ro/register/confirm/")) {
                data = data.replace("https://getreal.stage.bixel.ro/register/confirm/", "")
                activityIntent.putExtra("TOKEN", data)
                startActivity(activityIntent)
            } else if (data != null && data.contains("https://getreal.stage.bixel.ro/my-account/my-profile/confirm-email-update/")) {
                data = data.replace("https://getreal.stage.bixel.ro/my-account/my-profile/confirm-email-update/", "")
                val values = data.split("?target=")
                Log.e("SPLASH", values.toString())
                activityIntent.putExtra("TOKEN", values[0])
                activityIntent.putExtra("TARGET", values[1])
                startActivity(activityIntent)
            }

            startActivity(activityIntent)
            finish()
        } else {
            startActivity(Intent(
                    this,
                    MainActivity::class.java
            ))
            finish()
        }
    }
}