package md.lebo.getreal.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.facebook.CallbackManager
import com.github.windsekirun.rxsociallogin.RxSocialLogin
import com.github.windsekirun.rxsociallogin.intenal.model.PlatformType
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.Scope
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.feedback_layout.*
import md.lebo.getreal.R
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import pyxis.uzuki.live.richutilskt.utils.getJSONString
import java.util.*
import kotlin.collections.HashMap
import android.content.pm.PackageManager
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.TwitterAuthProvider
import com.twitter.sdk.android.core.Callback
import com.twitter.sdk.android.core.Result
import com.twitter.sdk.android.core.TwitterException
import com.twitter.sdk.android.core.TwitterSession
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class LoginActivity : AppCompatActivity() {
    private lateinit var loginResponse: Disposable
    private val grApiService: GRAPIService by lazy {
        GRAPIService.create()
    }
    private val TAG = "TAG_LOGIN_ACTIVTY"
    private lateinit var auth: FirebaseAuth

    private val compositeDisposable = CompositeDisposable()
    private lateinit var mHud: KProgressHUD
    private lateinit var mGoogleSignInClient: GoogleSignInClient

    private val RC_GET_TOKEN: Int = 4639

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mHud = Utils.createProgressHud(this)
        val token = intent.extras?.getString("TOKEN", "")
        val target = intent.extras?.getString("TARGET", "")

        //configure Google sign in
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Scope(Scopes.DRIVE_APPFOLDER))
                .requestServerAuthCode("683780339171-bgik72fft2df7hah612c8kpp9rn4a748.apps.googleusercontent.com")
                .requestIdToken("683780339171-bgik72fft2df7hah612c8kpp9rn4a748.apps.googleusercontent.com")
                .requestEmail()
                .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        auth = FirebaseAuth.getInstance()
        auth.signOut()
        println("INTENT: ${intent.extras}, $token, $target")
        if ((token != null && token.isNotEmpty()) && target.isNullOrEmpty()) {
            GRPrefs.clear()
            confirmAccount(token)
        } else if ((token != null && token.isNotEmpty()) && (target != null && target.isNotEmpty())) {
            confirmEmailChange(token, target)
        }

        inscriptionButton.setOnClickListener {
            startActivity(Intent(
                    this,
                    SignUpActivity::class.java
            ))
        }

        registerButton.setOnClickListener {
            if (emailInput.text.toString().isNotEmpty() &&
                    passwordInput.text.toString().isNotEmpty()) {
                loginUser(emailInput.text.toString(), passwordInput.text.toString())
            }
        }

        cancelButton.setOnClickListener {
            startActivity(Intent(
                    this,
                    MainActivity::class.java
            ))
        }

        customFacebookButton.setOnClickListener {
            RxSocialLogin.logout(PlatformType.FACEBOOK)
            RxSocialLogin.login(PlatformType.FACEBOOK)
        }

        connectWithGoogle.setOnClickListener {
            val signInIntent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_GET_TOKEN)
        }

        connectWithTwitter?.callback = object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>?) {
                Log.d(TAG, "twitterLogin:success$result")
                val data = result!!.data
                val values = HashMap<String, RequestBody>()
                values["oauth_token"] = RequestBody.create(MediaType.parse("text/plain"), data.authToken.token)
                values["oauth_token_secret"] = RequestBody.create(MediaType.parse("text/plain"), data.authToken.secret)
                connectViaService("twitter", values)
            }

            override fun failure(exception: TwitterException?) {
                Log.w(TAG, "twitterLogin:failure$exception")
            }
        }

        RxSocialLogin.initialize(this)
        RxSocialLogin.result(this)
                .subscribe({ result ->
                    println("RESULT LOGIN: $result")
                    val values = HashMap<String, RequestBody>()
                    when (result.platform) {
                        PlatformType.FACEBOOK -> {
                            values["access_token"] = RequestBody.create(
                                    MediaType.parse("text/plain"),
                                    JSONObject(result.accessToken).getJSONString("token"))
                            connectViaService("facebook", values)
                        }
                        else -> {
                        }
                    }
                }, {
                    Log.e("LOGIN_ACTIVITY", it.message)
                }).addTo(compositeDisposable)
    }

    override fun onStart() {
        super.onStart()
        mGoogleSignInClient.signOut()
    }

    override fun onResume() {
        super.onResume()
        if (GRPrefs.apiToken.isNotEmpty() && intent.extras?.getString("TARGET", "").isNullOrEmpty()) {
            Utils.feedbackMessage(
                    feedbackSnackbar,
                    "Successfully logged in"
            ) {
                startActivity(Intent(
                        this,
                        MainActivity::class.java
                ))
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        RxSocialLogin.activityResult(requestCode, resultCode, data)
        connectWithTwitter.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_GET_TOKEN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val result = task.getResult(ApiException::class.java)
                val values = HashMap<String, RequestBody>()
                if (result != null) {
                    values["code"] = RequestBody.create(
                            MediaType.parse("text/plain"), result.serverAuthCode!!)
                    values["client_id"] = RequestBody.create(
                            MediaType.parse("text/plain"), getString(R.string.google_client_id))
                    values["client_secret"] = RequestBody.create(
                            MediaType.parse("text/plain"), "v6i7hY19B3XcZ1wYy2at5HVX")
                    values["redirect_uri"] = RequestBody.create(
                            MediaType.parse("text/plain"), "urn:ietf:wg:oauth:2.0:oob")
                    values["grant_type"] = RequestBody.create(
                            MediaType.parse("text/plain"), "authorization_code")
                    getGoogleAccessToken(values)
                }
            } catch (e: ApiException) {
                Log.w("LOGIN", "handleSignInResult:error", e)
            }
        }
    }

    private fun connectViaService(service: String, values: HashMap<String, RequestBody>) {
        mHud.show()
        loginResponse = grApiService.connectViaService(service, values)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    GRPrefs.apiToken = result.getAsJsonPrimitive("token").asString
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            "Successfully logged in."
                    ) {
                        startActivity(Intent(
                                this,
                                MainActivity::class.java)
                        )
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    private fun getGoogleAccessToken(values: HashMap<String, RequestBody>) {
        mHud.show()
        loginResponse = grApiService.getGoogleAccessToken(options = values)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    val accessToken = result.getAsJsonPrimitive("access_token").asString
                    println("ACCESS_TOKEN $accessToken")
                    val data = HashMap<String, RequestBody>()
                    data["access_token"] = RequestBody.create(
                            MediaType.parse("text/plain"), accessToken)
                    connectViaService("google", data)
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    private fun handleTwitterSession(session: TwitterSession) {
        Log.d(TAG, "handleTwitterSession:$session")

        val credential = TwitterAuthProvider.getCredential(
                session.authToken.token,
                session.authToken.secret)

        auth.signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "signInWithCredential:success")
                        val user = auth.currentUser
                        println("USER: $user")
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithCredential:failure", task.exception)
                        Toast.makeText(baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                    }
                }
    }

    private fun getTwitterOAuth(values: HashMap<String, String>) {
        mHud.show()
        loginResponse = grApiService.getTwitterSignature(headerOptions = values)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    println("RESULT TWITTER: $result")
                    if (mHud.isShowing) mHud.dismiss()
                    //                    connectViaService("twitter", data)
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    private fun loginUser(email: String, password: String) {
        mHud.show()
        loginResponse = grApiService.loginUser(
                email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    GRPrefs.apiToken = result.getAsJsonPrimitive("token").asString
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            "Successfully logged in."
                    ) {
                        emailInput.setText("")
                        passwordInput.setText("")
                        startActivity(Intent(
                                this,
                                MainActivity::class.java)
                        )
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    private fun confirmAccount(token: String) {
        mHud.show()
        loginResponse = grApiService.confirmAccount(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (mHud.isShowing) mHud.dismiss()
                    val firstName = result
                            .getAsJsonObject("user")
                            .getAsJsonPrimitive("firstName").asString
                    if (firstName != null) {
                        Utils.feedbackMessage(
                                feedbackSnackbar,
                                "$firstName, your account was confirmed successfully. Now you can login into your account."
                        ) {}
                    } else {
                        Utils.feedbackMessage(
                                feedbackSnackbar,
                                "Your account was confirmed successfully. Now you can login into your account."
                        ) {}
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })

    }

    private fun confirmEmailChange(confirmationToken: String, target: String) {
        mHud.show()
        loginResponse = grApiService.confirmEmailChange(GRPrefs.apiToken, confirmationToken, target)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            "Your email change was confirmed successfully. Now you can login into your account."
                    ) {}
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })

    }
}
