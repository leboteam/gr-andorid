package md.lebo.getreal.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.gson.JsonParser
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.feedback_layout.*
import md.lebo.getreal.R
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.Utils
import retrofit2.HttpException

class SignUpActivity : AppCompatActivity() {
    private lateinit var registerResponse: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private lateinit var mHud: KProgressHUD

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        mHud = Utils.createProgressHud(this)

        cancelButton.setOnClickListener {
            finish()
        }

        registerButton.setOnClickListener {
            sendVerifyEmail(
                    firstnameInput.text.toString(),
                    lastnameInput.text.toString(),
                    emailInput.text.toString(),
                    passwordInput.text.toString(),
                    repeatPasswordInput.text.toString()
            )
        }

        loginButton.setOnClickListener {
            finish()
        }
    }

    private fun sendVerifyEmail(
            firstName: String,
            lastName: String,
            email: String,
            password: String,
            passwordSecond: String) {
        mHud.show()
        registerResponse = grApiService.registerUser(
                email,
                firstName,
                lastName,
                password,
                passwordSecond)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            result.getAsJsonPrimitive("message").toString()
                    ) {
                        finish()
                    }
                },
                        { error ->
                            if (mHud.isShowing) mHud.dismiss()
                            try {
                                val errorResponse = JsonParser().parse(
                                        (error as HttpException).response().errorBody()!!.string())
                                        .asJsonObject
                                        .getAsJsonObject("error")
                                        .getAsJsonObject("details")
                                        .getAsJsonArray("email")
                                        .get(0).asString
                                when (errorResponse.isNotEmpty()) {
                                    errorResponse.contains("e-mail") ->
                                        Utils.feedbackMessage(
                                                feedbackSnackbar,
                                                errorResponse
                                        ) {}
                                    else -> {
                                        Utils.feedbackMessage(
                                                feedbackSnackbar,
                                                Utils.requestErrorHandler(error)
                                        ) {}
                                    }
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }

                )

    }
}
