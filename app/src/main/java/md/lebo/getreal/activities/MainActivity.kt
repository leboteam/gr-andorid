package md.lebo.getreal.activities

import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.LayoutInflater
import com.ashokvarma.bottomnavigation.BottomNavigationBar
import com.ashokvarma.bottomnavigation.BottomNavigationItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.feedback_layout.*
import kotlinx.android.synthetic.main.welcome_layout.view.*
import md.lebo.getreal.R
import md.lebo.getreal.fragments.AboutFragment
import md.lebo.getreal.fragments.clubs.ClubsFragment
import md.lebo.getreal.fragments.events.EventsFragment
import md.lebo.getreal.fragments.profile.ProfileMainFragment
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class MainActivity : AppCompatActivity() {

    private lateinit var mActionBar: ActionBar
    private lateinit var userResponse: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupToolbar()

        Utils.loadFragment(R.id.mainFragmentContainer, EventsFragment(), fragmentManager)

        fragmentManager.addOnBackStackChangedListener {
            if (fragmentManager.backStackEntryCount > 0) {
                mActionBar.setDisplayHomeAsUpEnabled(true)
                barOptions.setNavigationOnClickListener { onBackPressed() }
            } else {
                mActionBar.setDisplayHomeAsUpEnabled(false)
            }
        }
        bottomBar
                .setBackgroundStyle(BottomNavigationBar.BACKGROUND_STYLE_STATIC)
                .addItem(BottomNavigationItem(R.drawable.marker_bottom, "Événements"))
                .addItem(BottomNavigationItem(R.drawable.map, "Clubs"))
                .addItem(BottomNavigationItem(R.drawable.about, "À propos"))
                .addItem(BottomNavigationItem(R.drawable.user, "Profil"))
                .initialise()

        bottomBar.setTabSelectedListener(object : BottomNavigationBar.OnTabSelectedListener {
            override fun onTabReselected(position: Int) {
                when (position) {
                    0 -> {
                        Utils.loadFragment(
                                R.id.mainFragmentContainer,
                                EventsFragment(),
                                fragmentManager)
                    }
                    1 -> {
                        Utils.loadFragment(
                                R.id.mainFragmentContainer,
                                ClubsFragment(),
                                fragmentManager)
                    }
                    2 -> {
                        Utils.loadFragment(
                                R.id.mainFragmentContainer,
                                ClubsFragment(),
                                fragmentManager)
                    }
                    3 -> {
                        Utils.loadFragment(
                                R.id.mainFragmentContainer,
                                ProfileMainFragment(),
                                fragmentManager)
                    }
                }
            }

            override fun onTabUnselected(position: Int) {
                //no implementation needed
            }

            override fun onTabSelected(position: Int) {
                when (position) {
                    0 -> {
                        Utils.loadFragment(
                                R.id.mainFragmentContainer,
                                EventsFragment(),
                                fragmentManager)
                    }
                    1 -> {
                        Utils.loadFragment(
                                R.id.mainFragmentContainer,
                                ClubsFragment(),
                                fragmentManager)
                    }
                    2 -> {
                        Utils.loadFragment(
                                R.id.mainFragmentContainer,
                                AboutFragment(),
                                fragmentManager)
                    }
                    3 -> {
                        Utils.loadFragment(
                                R.id.mainFragmentContainer,
                                ProfileMainFragment(),
                                fragmentManager)
                    }
                }
            }
        })

        getUserId()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        Utils.checkLocationPermissions(this, feedbackSnackbar) {
            if (!GRPrefs.welcomePopupShowed) {
                showWelcomeDialog()
            }
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(barOptions)
        mActionBar = supportActionBar!!
    }

    private fun showWelcomeDialog() {
        val inflater = LayoutInflater.from(this@MainActivity)
        val welcomeLayout = inflater.inflate(R.layout.welcome_layout, null)
        val alertBuilder = AlertDialog.Builder(this@MainActivity).create()
        alertBuilder.setView(welcomeLayout)
        alertBuilder.setCancelable(false)

        welcomeLayout.continueButton.setOnClickListener {
            GRPrefs.welcomePopupShowed = true
            alertBuilder.dismiss()
        }

        welcomeLayout.continueReadBtn.setOnClickListener {
            bottomBar.selectTab(2)
            GRPrefs.welcomePopupShowed = true
            alertBuilder.dismiss()
        }

        alertBuilder.show()
    }

    private fun showAppExitDialog() {
        val builder = android.app.AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.app_name))
        builder.setMessage(getString(R.string.close_app_msg))

        builder.setPositiveButton(getString(R.string.yes_action)) { _, _ ->
            GRPrefs.clear()
            finish()
        }

        builder.setNegativeButton(getString(R.string.no_action)) { _, _ ->
        }

        val dialog = builder.create()
        dialog.show()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (event!!.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (fragmentManager.backStackEntryCount > 0) {
                        fragmentManager.popBackStack()
                    } else {
                        showAppExitDialog()
                    }
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun getUserId() {
        userResponse = grApiService.getUserProfile(
                "1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a", GRPrefs.apiToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result != null) {
                        val user = result.getAsJsonObject("user")
                        val userID = user.getAsJsonPrimitive("id").asInt
                        if (userID > 0) {
                            GRPrefs.userId = userID
                        }
                    }
                }, { error ->
                    Utils.requestErrorHandler(error)
                })
    }
}
