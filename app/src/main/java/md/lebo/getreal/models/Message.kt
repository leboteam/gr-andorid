package md.lebo.getreal.models

data class Message(
        val id: Int,
        val profileImage: Int,
        val userName: String,
        val messageValue: String,
        val messageDate: String
)