package md.lebo.getreal.models.comments

import md.lebo.getreal.models.Author

data class CommentsData(
    val canRate: Boolean,
    val comments: List<Comment>
)

data class Comment(
        val author: Author,
        val body: String,
        val createdAt: String,
        val id: Int,
        val parent: Any,
        val rating: Any,
        val relativeTime: String,
        val replies: Int,
        val updatedAt: Any
)

data class Author(
    val firstName: String,
    val id: Int,
    val image: String
)