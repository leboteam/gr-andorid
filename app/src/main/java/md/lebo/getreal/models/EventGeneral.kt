package md.lebo.getreal.models

data class EventGeneral(
        val eventId: Int,
        val eventProfileImg: Int,
        val eventBackground: String,
        val eventTitle: String,
        val eventLocation: String,
        val eventDate: String,
        val eventPrice: Double,
        val eventTime: String,
        val eventMembersCount: Int)