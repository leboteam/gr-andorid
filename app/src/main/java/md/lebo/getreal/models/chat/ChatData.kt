package md.lebo.getreal.models.chat

data class ChatData(
    val messages: List<Message>
)

data class Message(
    val author: Author,
    val body: String,
    val conversation_id: Int,
    val createdAt: String,
    val id: Int,
    val relativeCreatedAt: String
)

data class Author(
    val firstName: String,
    val id: Int,
    val image: String
)