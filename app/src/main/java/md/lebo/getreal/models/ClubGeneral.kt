package md.lebo.getreal.models

data class ClubGeneral(
        val clubId: Int,
        val clubBackground: String,
        val clubProfileImg: Int,
        val clubTitle: String,
        val clubLocation: String,
        val clubMembersCount: Int,
        val clubPrice: Double
)