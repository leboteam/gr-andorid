package md.lebo.getreal.models

data class NotificationsData(
        val notifications: List<Notification>
)

data class Notification(
        val actor: String,
        val createdAt: String,
        val id: Int,
        val image: String,
        val readAt: String,
        val subject: String,
        val target: Target,
        val title: String,
        val type: String
)

data class Target(
        val id: String,
        val slug: String,
        val title: String,
        val type: String,
        val url: String
)