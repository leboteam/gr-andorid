package md.lebo.getreal.models.events

data class EventsData(
        val totalCount: Int,
        val events: List<Event>
)

data class Event(
        val id: Int,
        val type: String,
        val slug: String,
        val title: String,
        val image: String,
        val author: Author,
        val lat: Double,
        val lng: Double,
        val address: String,
        val startsAt: String,
        val programEndsAt: Any,
        val duration: String,
        val price: String,
        val places: Int,
        val description: String,
        val club: Any,
        val rating: Any,
        val participants: Int,
        val programHours: List<Any>,
        val sessions: List<String>,
        val participantCategories: List<Any>
)

data class Author(
        val id: Int,
        val firstName: String,
        val image: String
)