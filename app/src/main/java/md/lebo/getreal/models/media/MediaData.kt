package md.lebo.getreal.models.media

data class MediaData(
    val media: List<Media>
)

data class Media(
    val author: Author,
    val `file`: String,
    val id: Int,
    val thumb: String,
    val type: String
)

data class Author(
    val firstName: String,
    val id: Int,
    val image: String
)