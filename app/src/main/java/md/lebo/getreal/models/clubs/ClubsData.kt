package md.lebo.getreal.models.clubs

import md.lebo.getreal.models.comments.Author


data class ClubsData(
        val totalCount: Int,
        val clubs: List<Club>
)

data class Club(
        val id: Int,
        val slug: String,
        val title: String,
        val image: String,
        val description: String,
        val author: Author,
        val lat: Double,
        val lng: Double,
        val website: String,
        val address: String,
        val openingHours: String,
        val price: String,
        val rating: Any,
        val memberships: Int
)

data class Author(
        val id: Int,
        val firstName: String,
        val image: String
)