package md.lebo.getreal.models

import md.lebo.getreal.models.comments.Author


data class MessageData(
    val conversations: List<Conversation>
)

data class Conversation(
    val id: Int,
    val type: String,
    val createdAt: String,
    val me: Me,
    val members: List<Member>,
    val lastMessage: LastMessage
)

data class Member(
    val id: Int,
    val firstName: String,
    val image: String
)

data class LastMessage(
    val id: Int,
    val conversation_id: Int,
    val body: String,
    val createdAt: String,
    val relativeCreatedAt: String,
    val author: Author
)

data class Author(
    val id: Int,
    val firstName: String,
    val image: String
)

data class Me(
    val read: Boolean,
    val readAt: String
)