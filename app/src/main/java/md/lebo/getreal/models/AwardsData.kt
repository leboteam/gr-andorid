package md.lebo.getreal.models


data class AwardsData(
    val awards: List<Award>
)

data class Award(
    val id: Int,
    val user: User,
    val title: String,
    val club: Club,
    val competition: String,
    val date: String,
    val location: String
)

data class Club(
    val id: Int,
    val title: String
)

data class User(
    val id: Int,
    val firstName: String,
    val image: String
)