package md.lebo.getreal.map

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.clubs_marker_info_layout.view.*
import md.lebo.getreal.R
import md.lebo.getreal.models.clubs.Club

class ClubsInfoAdapter(private val context: Activity) : GoogleMap.InfoWindowAdapter {

    override fun getInfoWindow(marker: Marker?): View? {
        return null
    }

    override fun getInfoContents(marker: Marker?): View {
        val clubLayout = context.layoutInflater.inflate(R.layout.clubs_marker_info_layout, null)
        val obj = marker?.tag as Club
        Picasso.get().load(obj.image).into(object : Target {
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                clubLayout.clubImage.setImageResource(R.drawable.default_event)
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                clubLayout.clubImage.setImageResource(R.drawable.default_event)
            }

            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                clubLayout.clubImage.setImageBitmap(bitmap)
            }
        })
        clubLayout.clubTitle.text = obj.title
        clubLayout.locationTxt.text = obj.address
        clubLayout.clubLeaderTxt.text = obj.author.firstName
        return clubLayout
    }
}