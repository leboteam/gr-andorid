package md.lebo.getreal.map

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.events_marker_info_layout.view.*
import md.lebo.getreal.R
import md.lebo.getreal.models.events.Event

class EventsInfoAdapter(private val context: Activity) : GoogleMap.InfoWindowAdapter {
    override fun getInfoWindow(marker: Marker?): View? {
        return null
    }

    override fun getInfoContents(marker: Marker?): View {
        val markerLayout = context.layoutInflater.inflate(R.layout.events_marker_info_layout, null)
        val obj = marker?.tag as Event
        Picasso.get().load(obj.image).into(object : Target {
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                markerLayout.eventImage.setImageResource(R.drawable.default_event)
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                markerLayout.eventImage.setImageResource(R.drawable.default_event)
            }

            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                markerLayout.eventImage.setImageBitmap(bitmap)
            }
        })
        markerLayout.eventTitle.text = obj.title
        markerLayout.eventAuthor.text = obj.author.firstName
        markerLayout.eventStartsAtText.text = obj.startsAt
        markerLayout.eventTimeTxt.text = obj.duration
        return markerLayout
    }
}