package md.lebo.getreal.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.message_item_layout.view.*
import md.lebo.getreal.R
import md.lebo.getreal.models.chat.ChatData
import md.lebo.getreal.models.chat.Message

class ChatAdapter(private val messages: ChatData) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MessagesViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.message_item_layout, parent, false))
    }

    override fun getItemCount() = messages.messages.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MessagesViewHolder).bind(messages.messages[position])
    }

    class MessagesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(message: Message) = with(itemView) {
            itemView.msgProfileImage.setImageURI(message.author.image)
            itemView.msgSender.text = message.author.firstName
            itemView.msgSendTime.text = message.createdAt
            itemView.msgText.text = message.body
        }
    }
}