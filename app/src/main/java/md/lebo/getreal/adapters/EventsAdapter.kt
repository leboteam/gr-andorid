package md.lebo.getreal.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.event_layout.view.*
import md.lebo.getreal.R
import md.lebo.getreal.models.events.Event

class EventsAdapter(
        private var events: ArrayList<Event>,
        private val eventListener: (Event) -> Unit) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return EventsViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.event_layout, parent, false))
    }

    override fun getItemCount() = events.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as EventsViewHolder).bind(events[position], eventListener)
    }

    fun addEvents(list: List<Event>) {
        events.addAll(list)
        notifyDataSetChanged()
    }

    class EventsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(event: Event, listener: (Event) -> Unit) = with(itemView) {
            eventBackground.setImageURI(event.image)
            eventProfileImg.setImageURI(event.author.image)
            eventTitle.text = event.title
            locationEventTxt.text = event.address
            eventDateTxt.text = event.startsAt
            eventTimeTxt.text = event.duration
            eventPriceTxt.text = "${event.price} €"
            eventMembersTxt.text = event.participants.toString()
            setOnClickListener { listener(event) }
        }
    }

}