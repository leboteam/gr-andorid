package md.lebo.getreal.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.media_item_layout.view.*
import md.lebo.getreal.R
import md.lebo.getreal.models.media.Media
import md.lebo.getreal.models.media.MediaData

class MediaAdapter(
        private var mediaData: MediaData,
        private val eventListener: (Media) -> Unit) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return EventsViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.media_item_layout, parent, false))
    }

    override fun getItemCount() = mediaData.media.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as EventsViewHolder).bind(mediaData.media[position], eventListener)
    }

    class EventsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(media: Media, listener: (Media) -> Unit) = with(itemView) {
            itemView.mediaThumbnail.setImageURI(media.thumb)
            itemView.mediaAuthor.text = media.author.firstName
            itemView.mediaAuthorImage.setImageURI(media.author.image)
            setOnClickListener { listener(media) }
        }
    }
}
