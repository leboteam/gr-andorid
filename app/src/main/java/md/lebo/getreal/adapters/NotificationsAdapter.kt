package md.lebo.getreal.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.notification_item_layout.view.*
import md.lebo.getreal.R
import md.lebo.getreal.models.Notification
import md.lebo.getreal.models.NotificationsData

class NotificationsAdapter(private val notifications: NotificationsData) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NotificationViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.notification_item_layout, parent, false))
    }

    override fun getItemCount() = notifications.notifications.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as NotificationViewHolder).bind(notifications.notifications[position])
    }

    class NotificationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(notification: Notification) = with(itemView) {
            itemView.notificationProfileImage.setImageURI(notification.image)
            itemView.notificationTitle.text = notification.title
            itemView.notificationTime.text = notification.createdAt
            itemView.notificationType.text = notification.type
        }
    }
}