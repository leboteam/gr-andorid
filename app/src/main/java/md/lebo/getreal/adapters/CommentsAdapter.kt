package md.lebo.getreal.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.message_item_layout.view.*
import md.lebo.getreal.R
import md.lebo.getreal.models.comments.Comment
import md.lebo.getreal.models.comments.CommentsData

class CommentsAdapter(private val comments: CommentsData) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CommentsViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.message_item_layout, parent, false))
    }

    override fun getItemCount() = comments.comments.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CommentsViewHolder).bind(comments.comments[position])
    }

    class CommentsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(comment: Comment) = with(itemView) {
            itemView.msgProfileImage.setImageURI(comment.author.image)
            itemView.msgSender.text = comment.author.firstName
            itemView.msgSendTime.text = comment.createdAt
            itemView.msgText.text = comment.body
        }
    }
}