package md.lebo.getreal.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.club_layout.view.*
import md.lebo.getreal.R
import md.lebo.getreal.models.clubs.Club

class ClubsAdapter(
        private var clubs: ArrayList<Club>,
        private val clubListener: (Club) -> Unit) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ClubsViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.club_layout, parent, false))
    }

    override fun getItemCount() = clubs.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ClubsViewHolder).bind(clubs[position], clubListener)
    }

    fun addClubs(list: List<Club>) {
        clubs.addAll(list)
        notifyDataSetChanged()
    }

    class ClubsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(club: Club, listener: (Club) -> Unit) = with(itemView) {
            clubBackground.setImageURI(club.image)
            clubProfileImg.setImageURI(club.author.image)
            clubTitle.text = club.title
            clubLocation.text = club.address
            clubMembersCount.text = "Members: ${club.memberships}"
            clubPrice.text = "Price: ${club.price} €"
            setOnClickListener { listener(club) }
        }
    }

}