package md.lebo.getreal.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.message_list_item.view.*
import md.lebo.getreal.R
import md.lebo.getreal.models.Conversation
import md.lebo.getreal.models.Member
import md.lebo.getreal.models.MessageData

class MessageAdapter(
        private val messageData: MessageData,
        private val messageListener: (Conversation) -> Unit) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ClubsViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.message_list_item, parent, false))
    }

    override fun getItemCount() = messageData.conversations.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ClubsViewHolder).bind(
                messageData.conversations[position].members[0],
                messageData.conversations[position],
                messageListener)
    }

    class ClubsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(member: Member, conversation: Conversation, listener: (Conversation) -> Unit) = with(itemView) {
            itemView.msgProfileImage.setImageURI(member.image)
            itemView.msgSender.text = member.firstName
            itemView.msgText.text = conversation.lastMessage.body
            itemView.msgSendTime.text = conversation.createdAt
            setOnClickListener { listener(conversation) }
        }
    }

}