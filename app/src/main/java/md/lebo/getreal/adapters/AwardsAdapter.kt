package md.lebo.getreal.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.award_layout.view.*
import md.lebo.getreal.R
import md.lebo.getreal.models.Award
import md.lebo.getreal.models.AwardsData

class AwardsAdapter(
        private val awards: AwardsData) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AwardsViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.award_layout, parent, false))
    }

    override fun getItemCount() = awards.awards.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AwardsViewHolder).bind(awards.awards[position])
    }

    class AwardsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(award: Award) = with(itemView) {
            awardTitle.text = award.title
            awardDescription.text = award.competition
            awardClub.text = award.club.title
            awardDate.text = award.date
            awardDescriptionRepeat.text = award.competition
        }
    }
}