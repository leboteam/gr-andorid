package md.lebo.getreal.utils

import android.Manifest
import android.app.Activity
import android.app.Fragment
import android.app.FragmentManager
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.google.android.gms.maps.model.Circle
import com.kaopiz.kprogresshud.KProgressHUD
import md.lebo.getreal.R
import md.lebo.getreal.gr.GetReal
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable
import retrofit2.HttpException
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.concurrent.TimeoutException

object Utils {
    const val LOCATION_PERMISSION_CODE: Int = 10001
    const val CAMERA_PERMISSION_CODE: Int = 10002
    const val OPEN_CAMERA_CODE: Int = 10002
    const val SELECT_PHOTO_CODE: Int = 10004
    const val TAKE_PHOTO_CODE: Int = 10005

    fun loadFragmentWithBackStack(
            @NonNull containerId: Int,
            @NotNull fragment: Fragment,
            @NonNull paramFragmentManager: FragmentManager,
            @Nullable booleanArgument: Boolean = false) {

        if (booleanArgument) {
            val bundle = Bundle()
            bundle.putBoolean("IS_MINE", booleanArgument)
            fragment.arguments = bundle
        }
        paramFragmentManager
                .beginTransaction()
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commit()
    }

    fun loadFragment(
            @NonNull containerId: Int,
            @NonNull fragment: Fragment,
            @NonNull paramFragmentManager: FragmentManager) {
        if (paramFragmentManager.backStackEntryCount > 0) {
            paramFragmentManager.popBackStack()
        }

        paramFragmentManager
                .beginTransaction()
                .replace(containerId, fragment)
                .commit()
    }

    fun requestErrorHandler(error: Throwable): String {
        return when (error) {
            is IOException -> GetReal.mInstance.getString(R.string.error_no_internet_connection)
            is TimeoutException -> GetReal.mInstance.getString(R.string.error_timeout)
            is HttpException -> {
                when (error.code()) {
                    401 -> GetReal.mInstance.getString(R.string.error_401)
                    400 -> GetReal.mInstance.getString(R.string.error_400)
                    403 -> GetReal.mInstance.getString(R.string.error_403)
                    404 -> GetReal.mInstance.getString(R.string.error_404)
                    500 -> GetReal.mInstance.getString(R.string.error_500)
                    else -> GetReal.mInstance.getString(R.string.unknown_error)
                }
            }
            else -> {
                GetReal.mInstance.getString(R.string.unknown_error)
            }
        }
    }

    fun feedbackMessage(
            layout: CoordinatorLayout,
            messageValue: String,
            callbackMethod: () -> Unit) {
        Snackbar.make(
                layout,
                messageValue,
                Snackbar.LENGTH_SHORT
        ).addCallback(object : Snackbar.Callback() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                super.onDismissed(transientBottomBar, event)
                callbackMethod()
            }
        }).show()
    }

    fun createProgressHud(context: Context): KProgressHUD {
        return KProgressHUD(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(false)
    }

    fun convertBitmapToJpeg(bitmap: Bitmap?, context: Activity, fileName: String): File {
        val file = File(context.cacheDir, fileName)
        file.createNewFile()

        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val data = byteArrayOutputStream.toByteArray()

        val fileOutputStream = FileOutputStream(file)
        fileOutputStream.write(data)
        fileOutputStream.flush()
        fileOutputStream.close()
        return file
    }

    fun getZoomLevel(circle: Circle?): Int {
        val radius = circle!!.radius
        val scale = radius / 500
        return (16 - Math.log(scale) / Math.log(2.0)).toInt()
    }

    fun getMultiPartData(file: File, fileType: String): MultipartBody.Part {
        val reqFile = RequestBody.create(MediaType.parse("image/*"), file)
        return MultipartBody.Part.createFormData(fileType, file.name, reqFile)
    }

    fun checkLocationPermissions(context: Activity, feedbackLayout: CoordinatorLayout, callbackMethod: () -> Unit) {
        val fineLocationPermission = ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION)

        val coarseLocationPermission = ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION)

        if (fineLocationPermission != PackageManager.PERMISSION_GRANTED ||
                coarseLocationPermission != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                            Manifest.permission.ACCESS_COARSE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(context,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {

                Snackbar.make(
                        feedbackLayout,
                        "Your location is used to show events that are near you",
                        Snackbar.LENGTH_LONG).setAction("GRANT") {
                    ActivityCompat.requestPermissions(
                            context,
                            arrayOf(
                                    Manifest.permission.ACCESS_COARSE_LOCATION,
                                    Manifest.permission.ACCESS_FINE_LOCATION),
                            LOCATION_PERMISSION_CODE)
                }.addCallback(object : Snackbar.Callback() {
                    override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                        super.onDismissed(transientBottomBar, event)
                        ActivityCompat.requestPermissions(
                                context,
                                arrayOf(
                                        Manifest.permission.ACCESS_COARSE_LOCATION,
                                        Manifest.permission.ACCESS_FINE_LOCATION),
                                LOCATION_PERMISSION_CODE)
                    }
                }).show()
            } else {
                ActivityCompat.requestPermissions(
                        context,
                        arrayOf(
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION),
                        LOCATION_PERMISSION_CODE)
            }
        } else {
            callbackMethod()
        }
    }
}