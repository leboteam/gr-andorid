package md.lebo.getreal.utils

import com.chibatching.kotpref.KotprefModel

object GRPrefs : KotprefModel() {
    var apiKey: String by stringPref(default = "")
    var apiToken: String by stringPref(default = "")
    var userId: Int by intPref(default = 0)
    var welcomePopupShowed: Boolean by booleanPref(default = false)
}