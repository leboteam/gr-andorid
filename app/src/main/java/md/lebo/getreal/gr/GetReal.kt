package md.lebo.getreal.gr

import android.app.Application
import com.chibatching.kotpref.Kotpref
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.facebook.drawee.backends.pipeline.Fresco
import com.github.windsekirun.rxsociallogin.facebook.FacebookConfig
import com.github.windsekirun.rxsociallogin.initSocialLogin
import md.lebo.getreal.R


class GetReal : Application() {
    companion object {
        lateinit var mInstance: GetReal
            private set
    }

    override fun onCreate() {
        super.onCreate()
        mInstance = this
//        printHashKey(this)
        Kotpref.init(this)
        Fresco.initialize(this)
        FacebookSdk.sdkInitialize(this)
        AppEventsLogger.activateApp(this)
        initSocialLogin {
            facebook(getString(R.string.facebook_app_id)) {
                behaviorOnCancel = true
                requireWritePermissions = false
                requireEmail = true
                imageEnum = FacebookConfig.FacebookImageEnum.Large
            }
//            google(getString(R.string.google_client_id)) {
//                requireEmail = true
//                clientTokenId = "683780339171-1hdhdq8ckr65ks64help5eubuave38fo.apps.googleusercontent.com"
//            }
            twitter("pVyy5ppk4knmysqYbq7IFD3p1", "XXTYXwPVBZHJOKEqBwveQ5fO36eZONCYSwziy8P0r3IxTn1Nxv")
        }
    }

//    @SuppressLint("PackageManagerGetSignatures")
//    fun printHashKey(pContext: Context) {
//        try {
//            val info = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
//            for (signature in info.signatures) {
//                val md = MessageDigest.getInstance("SHA")
//                md.update(signature.toByteArray())
//                val hashKey = String(Base64.encode(md.digest(), 0))
//                Log.i("TAG", "printHashKey() Hash Key: $hashKey")
//            }
//        } catch (e: NoSuchAlgorithmException) {
//            Log.e("TAG", "printHashKey()", e)
//        } catch (e: Exception) {
//            Log.e("TAG", "printHashKey()", e)
//        }
//
//    }
}