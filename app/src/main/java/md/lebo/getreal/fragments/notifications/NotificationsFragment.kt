package md.lebo.getreal.fragments.notifications

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.feedback_layout.*
import kotlinx.android.synthetic.main.notifications_fragment.view.*
import md.lebo.getreal.R
import md.lebo.getreal.adapters.NotificationsAdapter
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class NotificationsFragment : BaseFragment() {
    private lateinit var notificationsResponse: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private lateinit var notificationsLayout: View
    private lateinit var mHud: KProgressHUD

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        notificationsLayout = inflater!!.inflate(R.layout.notifications_fragment, container, false)
        notificationsLayout.notificationsRecycler.setHasFixedSize(true)
        notificationsLayout.notificationsRecycler.layoutManager = LinearLayoutManager(
                activity,
                LinearLayoutManager.VERTICAL,
                false
        )
        mHud = Utils.createProgressHud(activity)
        getNotifications()
        return notificationsLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle("Notifications")
    }

    private fun getNotifications() {
        mHud.show()
        notificationsResponse = grApiService.getNotifications(
                GRPrefs.apiToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result != null) {
                        if (result.notifications.isNotEmpty()) {
                            if (mHud.isShowing) mHud.dismiss()
                            Utils.feedbackMessage(
                                    feedbackSnackbar,
                                    "No notifications do display."
                            ) {}
                        } else {
                            if (mHud.isShowing) mHud.dismiss()
                            notificationsLayout.notificationsRecycler.adapter = NotificationsAdapter(result)
                        }
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
        compositeDisposable.addAll(notificationsResponse)
    }

}