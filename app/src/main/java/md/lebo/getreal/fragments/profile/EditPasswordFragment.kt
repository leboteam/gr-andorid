package md.lebo.getreal.fragments.profile

import android.os.Bundle
import android.view.*
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.edit_password_fragment.view.*
import kotlinx.android.synthetic.main.feedback_layout.*
import md.lebo.getreal.R
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class EditPasswordFragment : BaseFragment() {

    private lateinit var passwordEditResponse: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private lateinit var mHud: KProgressHUD
    private lateinit var passwordEditView: View

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        passwordEditView = inflater!!.inflate(R.layout.edit_password_fragment, container, false)
        setHasOptionsMenu(true)
        mHud = Utils.createProgressHud(this@EditPasswordFragment.activity)
        return passwordEditView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.edit_password_title))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu!!.clear()
        inflater?.inflate(R.menu.add_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.addOption ->
                if (passwordEditView.profileEditCPassInput.text.toString().isNotEmpty() &&
                        passwordEditView.profileEditNPassInput.text.toString().isNotEmpty() &&
                        passwordEditView.profileEditRNPassInput.text.toString().isNotEmpty()) {
                    editPassword(
                            passwordEditView.profileEditNPassInput.text.toString(),
                            passwordEditView.profileEditRNPassInput.text.toString(),
                            passwordEditView.profileEditCPassInput.text.toString()
                    )
                }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun editPassword(
            newPassword: String,
            newPasswordRepeat: String,
            currentPassword: String) {
        mHud.show()
        passwordEditResponse = grApiService.changePassword(
                GRPrefs.apiToken,
                newPassword,
                newPasswordRepeat,
                currentPassword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            result.getAsJsonPrimitive("message").asString
                    ) { this@EditPasswordFragment.activity.fragmentManager.popBackStack() }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }
}