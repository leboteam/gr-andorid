package md.lebo.getreal.fragments.events

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.*
import android.widget.Toast
import com.ablanco.imageprovider.ImageProvider
import com.ablanco.imageprovider.ImageSource
import com.erikagtierrez.multiple_media_picker.Gallery
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.kaopiz.kprogresshud.KProgressHUD
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.event_create_fragment.view.*
import kotlinx.android.synthetic.main.feedback_layout.*
import kotlinx.android.synthetic.main.map_dialog_layout.*
import md.lebo.getreal.R
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.*

class EventCreateFragment : BaseFragment() {
    private var imageSelected: Bitmap? = null
    private var locationValue: LatLng? = null
    private lateinit var eventResponse: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private lateinit var mHud: KProgressHUD
    private lateinit var eventCreateLayout: View
    private val types = arrayOf("event", "program", "competition")
    val OPEN_MEDIA_PICKER = 1536
    val READ_STORAGE_PERMISSION_REQUEST_CODE = 1537

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        eventCreateLayout = inflater!!.inflate(R.layout.event_create_fragment, container, false)
        mHud = Utils.createProgressHud(activity)

        var isEdit = false
        var eventID = 0
        if (arguments != null && arguments.containsKey("IS_EDIT_EVENT") && arguments.containsKey("EVENT_EDIT_ID")) {
            isEdit = arguments.getBoolean("IS_EDIT_EVENT", false)
            eventID = arguments.getInt("EVENT_EDIT_ID", 0)
        }

        eventCreateLayout.eventCreateTakePhotoBtn.setOnClickListener {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(
                                Manifest.permission.CAMERA),
                        Utils.CAMERA_PERMISSION_CODE)
            } else {
                ImageProvider(activity).getImage(ImageSource.CAMERA) { bitmap ->
                    eventCreateLayout.eventCreateBackground.setImageBitmap(bitmap)
                    imageSelected = bitmap
                }
            }
        }

        eventCreateLayout.eventCreateSelectPhotoBtn.setOnClickListener {
            if (checkPermissionForReadExternalStorage()) {
                val intent = Intent(activity, Gallery::class.java)
                intent.putExtra("title", "Média")
                intent.putExtra("mode", 2)
                intent.putExtra("maxSelection", 1)
                startActivityForResult(intent, OPEN_MEDIA_PICKER)
            } else {
                requestPermissionForReadExternalStorage()
            }
        }

        eventCreateLayout.eventCreateShowMapBtn.setOnClickListener {
            showMapDialog(activity)
        }

        if (arguments != null && isEdit && eventID > 0) {
            getEvent(eventID)
        }

        return eventCreateLayout
    }

    private fun checkPermissionForReadExternalStorage(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val result = activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            return result == PackageManager.PERMISSION_GRANTED
        }
        return false
    }

    @Throws(Exception::class)
    private fun requestPermissionForReadExternalStorage() {
        try {
            ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    READ_STORAGE_PERMISSION_REQUEST_CODE)
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.create_event_title))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu!!.clear()
        inflater?.inflate(R.menu.add_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.addOption -> if (
                    eventCreateLayout.eventCreateTitle.text.isNotEmpty() &&
                    eventCreateLayout.eventCreateDescription.text.isNotEmpty() &&
                    eventCreateLayout.eventCreateStart.text.isNotEmpty() &&
                    eventCreateLayout.eventCreateTime.text.isNotEmpty() &&
                    eventCreateLayout.eventCreatePrice.text.isNotEmpty() &&
                    eventCreateLayout.eventCreatePlaceNumber.text.isNotEmpty() &&
                    eventCreateLayout.eventCreateTimeAmount.text.isNotEmpty() &&
                    eventCreateLayout.eventCreateAddressInput.text.isNotEmpty() &&
                    imageSelected != null &&
                    locationValue != null
            ) {
                val data = java.util.HashMap<String, RequestBody>()
                val location = locationValue!!
                var type = "event"
                if (arguments != null) {
                    type = when {
                        arguments.getBoolean("IS_PROGRAM", false) -> "program"
                        arguments.getBoolean("IS_COMPETITION", false) -> "competition"
                        else -> "event"
                    }
                }
                val body = Utils.getMultiPartData(Utils.convertBitmapToJpeg(
                        imageSelected,
                        this@EventCreateFragment.activity,
                        "GET_REAL_IMAGE"), "image")

                data["type"] = RequestBody.create(MediaType.parse("text/plain"), type)
                data["title"] = RequestBody.create(MediaType.parse("text/plain"), eventCreateLayout.eventCreateTitle.text.toString())
                data["lat"] = RequestBody.create(MediaType.parse("text/plain"), location.latitude.toString())
                data["lng"] = RequestBody.create(MediaType.parse("text/plain"), location.longitude.toString())
                data["address"] = RequestBody.create(MediaType.parse("text/plain"), eventCreateLayout.eventCreateAddressInput.text.toString())
                data["startsAt"] = RequestBody.create(MediaType.parse("text/plain"), eventCreateLayout.eventCreateStart.text.toString())
                data["duration"] = RequestBody.create(MediaType.parse("text/plain"), eventCreateLayout.eventCreateTimeAmount.text.toString())
                data["price"] = RequestBody.create(MediaType.parse("text/plain"), eventCreateLayout.eventCreatePrice.text.toString())
                data["places"] = RequestBody.create(MediaType.parse("text/plain"), eventCreateLayout.eventCreatePlaceNumber.text.toString())
                data["description"] = RequestBody.create(MediaType.parse("text/plain"), eventCreateLayout.eventCreateDescription.text.toString())
                if (arguments != null &&
                        arguments.getBoolean("IS_EVENT_EDIT", false) &&
                        arguments.getInt("EVENT_EDIT_ID", 0) > 0) {
                    editEvent(arguments.getInt("EVENT_EDIT_ID"), data, body)
                } else {
                    createEvent(data, body)
                }
            } else {
                Utils.feedbackMessage(
                        feedbackSnackbar,
                        "All fields must be completed!"
                ) {}
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getEvent(eventID: Int) {
        mHud.show()
        eventResponse = grApiService.getEvent(eventID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    println("EVENT RESULT: $result")
                    locationValue = LatLng(result.lat, result.lng)
                    eventCreateLayout.eventCreateTitle.setText(result.title)
                    eventCreateLayout.eventCreateBackground.setImageURI(result.image)
                    eventCreateLayout.eventCreateDescription.setText(result.description)
                    eventCreateLayout.eventCreateStart.setText(result.startsAt)
                    eventCreateLayout.eventCreateTime.setText(result.startsAt.substring(result.startsAt.length - 5, result.startsAt.length))
                    eventCreateLayout.eventCreatePrice.setText(result.price)
                    eventCreateLayout.eventCreateTimeAmount.setText(result.duration)
                    eventCreateLayout.eventCreatePlaceNumber.setText(result.places.toString())
                    eventCreateLayout.eventCreateAddressInput.setText(result.address)
                    if (mHud.isShowing) mHud.dismiss()
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>?,
            grantResults: IntArray?) {

        when (requestCode) {
            Utils.TAKE_PHOTO_CODE -> {
                val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (takePhotoIntent.resolveActivity(activity.packageManager) != null) {
                    startActivityForResult(takePhotoIntent, Utils.OPEN_CAMERA_CODE)
                }
                return
            }
            else -> {
                Utils.feedbackMessage(
                        feedbackSnackbar,
                        "It won't be possible to take photos"
                ) {}
            }
        }
    }

    private fun createEvent(eventHashMap: HashMap<String, RequestBody>, body: MultipartBody.Part) {
        mHud.show()
        eventResponse = grApiService.createEvent(
                GRPrefs.apiToken,
                eventHashMap,
                body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            println("RESULT: $result")
                            if (mHud.isShowing) mHud.dismiss()
                            activity.fragmentManager.popBackStack()
                        }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    private fun editEvent(eventID: Int, eventHashMap: HashMap<String, RequestBody>, body: MultipartBody.Part) {
        mHud.show()
        eventResponse = grApiService.editEvent(
                GRPrefs.apiToken,
                eventID,
                eventHashMap,
                body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            println("RESULT: $result")
                            if (mHud.isShowing) mHud.dismiss()
                            activity.fragmentManager.popBackStack()
                        }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    private fun showMapDialog(
            context: Activity) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.map_dialog_layout)
        dialog.show()

        MapsInitializer.initialize(context)
        dialog.dialogMap.onCreate(dialog.onSaveInstanceState())
        dialog.dialogMap.onResume()
        dialog.dialogMap.getMapAsync { googleMap ->
            if (locationValue != null) {
                println("LOCAL $locationValue")
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationValue, 16f))
                val oldMarker = MarkerOptions()
                oldMarker.position(locationValue as LatLng)
                googleMap.addMarker(oldMarker)
            }

            googleMap?.setOnMapLongClickListener { latLng ->
                println("LAT_LNG: $latLng")
                googleMap.clear()
                val marker = MarkerOptions()
                marker.position(latLng)
                googleMap.addMarker(marker)
                locationValue = latLng
                val geocoder = Geocoder(activity, Locale.getDefault())
                val addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
                eventCreateLayout.eventCreateAddressInput.setText(String.format(
                        "%s, %s, %s",
                        addresses[0].countryName,
                        addresses[0].locality,
                        addresses[0].getAddressLine(0)
                ))
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Utils.SELECT_PHOTO_CODE && resultCode == Activity.RESULT_OK) {
            try {
                val imageUri = data?.data
                val imageStream = activity.contentResolver.openInputStream(imageUri)
                imageSelected = BitmapFactory.decodeStream(imageStream)
                Picasso.get().load(data!!.data).rotate(90f).into(eventCreateLayout.eventCreateBackground)
            } catch (e: Throwable) {
                e.printStackTrace()
                Toast.makeText(this@EventCreateFragment.activity, "Something went wrong", Toast.LENGTH_LONG).show()
            }
        } else if (requestCode == Utils.TAKE_PHOTO_CODE && resultCode == Activity.RESULT_OK) {
            val extras = data?.extras
            imageSelected = extras!!.get("data") as Bitmap
            eventCreateLayout.eventCreateBackground.setImageBitmap(imageSelected)
        } else {
            Toast.makeText(this@EventCreateFragment.activity, "You haven't picked Image", Toast.LENGTH_LONG).show()
        }

        if (requestCode == OPEN_MEDIA_PICKER) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val selectionResult = data.getStringArrayListExtra("result")
                val imageFile = BitmapFactory.decodeFile(selectionResult[0])
                imageSelected = imageFile
                eventCreateLayout.eventCreateBackground.setImageBitmap(imageFile)
            }
        }
    }
}