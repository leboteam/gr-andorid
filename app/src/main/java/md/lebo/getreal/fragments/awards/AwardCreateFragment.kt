package md.lebo.getreal.fragments.awards

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.award_create_fragment.view.*
import kotlinx.android.synthetic.main.clubs_popup.view.*
import kotlinx.android.synthetic.main.feedback_layout.*
import md.lebo.getreal.R
import md.lebo.getreal.adapters.SimpleClubAdapter
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.models.clubs.Club
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils
import java.util.*
import kotlin.collections.HashMap

class AwardCreateFragment : BaseFragment() {
    private lateinit var awardsResponse: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private val compositeDisposable = CompositeDisposable()
    private lateinit var awardLayout: View
    private lateinit var mHud: KProgressHUD
    private lateinit var clubsDisposable: Disposable
    private val clubs: ArrayList<Club> = ArrayList()
    private var currentClubId: Int = 0
    private lateinit var mClubsListAdapter: SimpleClubAdapter
    private lateinit var currentAlertDialog: AlertDialog

    //pagination
    private val PAGE_SIZE = 10
    private var isLastPage = false
    private var mCurrentPage = 1
    private var isLoading = false

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        mHud = Utils.createProgressHud(activity)
        awardLayout = inflater!!.inflate(R.layout.award_create_fragment, container, false)
        awardLayout.awardClubInput.setOnClickListener {
            showClubsPopup()
        }

        return awardLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.create_award_title))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        inflater?.inflate(R.menu.add_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.addOption ->
                if (awardLayout.awardTitleInput.text.toString().isNotEmpty() &&
                        awardLayout.awardCompetitionInput.text.toString().isNotEmpty() &&
                        currentClubId > 0 &&
                        awardLayout.awardDateInput.text.toString().isNotEmpty() &&
                        awardLayout.awardAddressInput.text.toString().isNotEmpty()) {
                    val values = HashMap<String, String>()
                    values["title"] = awardLayout.awardTitleInput.text.toString()
                    values["competition"] = awardLayout.awardCompetitionInput.text.toString()
                    values["club"] = currentClubId.toString()
                    values["date"] = awardLayout.awardDateInput.text.toString()
                    values["location"] = awardLayout.awardAddressInput.text.toString()
                    createAward(values)
                } else {
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            "All fields must be completed."
                    ) {}
                }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun createAward(values: HashMap<String, String>) {
        mHud.show()
        awardsResponse = grApiService.createAward(GRPrefs.apiToken, values)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result != null) {
                        if (mHud.isShowing) mHud.dismiss()
                        Utils.feedbackMessage(
                                feedbackSnackbar,
                                "Award created successfully"
                        ) { activity.fragmentManager.popBackStack() }
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    private fun onClubClicked(club: Club) {
        if (currentAlertDialog.isShowing) currentAlertDialog.dismiss()
        awardLayout.awardClubInput.setText(club.title)
        currentClubId = club.id
        clubs.clear()
        mClubsListAdapter.notifyDataSetChanged()
    }

    private fun showClubsPopup() {
        val inflater = LayoutInflater.from(this@AwardCreateFragment.activity)
        val clubListLayout = inflater.inflate(R.layout.clubs_popup, null)
        val alertDialog = android.support.v7.app.AlertDialog.Builder(this@AwardCreateFragment.activity).create()
        currentAlertDialog = alertDialog
        clubListLayout.clubsPopupRecyclerView.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(
                this@AwardCreateFragment.activity,
                LinearLayoutManager.VERTICAL,
                false
        )
        clubListLayout.clubsPopupRecyclerView.layoutManager = linearLayoutManager
        clubListLayout.clubsPopupRecyclerView.addOnScrollListener(getScrollListener(linearLayoutManager))
        alertDialog.setView(clubListLayout)
        getClubs { clubListLayout.clubsPopupRecyclerView.adapter = mClubsListAdapter }
        alertDialog.show()
    }

    private fun getClubs(callback: () -> Unit) {
        val optionsMap = HashMap<String, String>()
        optionsMap["page"] = mCurrentPage.toString()
        optionsMap["length"] = PAGE_SIZE.toString()
        clubsDisposable = grApiService.getClubsList(optionsMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    println("RESULT: $result")
                    isLoading = false
                    if (result != null) {
                        if (result.clubs.isEmpty() && clubs.isEmpty()) {
                            Utils.feedbackMessage(
                                    feedbackSnackbar,
                                    "No clubs found!"
                            ) {}
                        } else {
                            val items = result.clubs
                            if (clubs.isEmpty()) {
                                clubs.addAll(result.clubs)
                                mClubsListAdapter = SimpleClubAdapter(clubs) { club: Club -> onClubClicked(club) }
                                callback()
                            } else {
                                if (items.isNotEmpty()) mClubsListAdapter.addClubs(items)
                                if (items.size >= PAGE_SIZE) {
                                    loadMoreItems()
                                } else {
                                    isLastPage = true
                                    mCurrentPage = 1
                                }
                            }
                        }
                    }
                }, { error ->
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
        compositeDisposable.add(clubsDisposable)
    }

    private fun getScrollListener(linearLayoutManager: LinearLayoutManager):
            RecyclerView.OnScrollListener {
        return object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val visibleItemCount = linearLayoutManager.childCount
                val totalItemCount = linearLayoutManager.itemCount
                val firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()

                if (!isLoading && !isLastPage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= PAGE_SIZE) {
                        loadMoreItems()
                    }
                }
            }
        }
    }


    private fun loadMoreItems() {
        isLoading = true
        mCurrentPage += 1
        getClubs {}
    }
}