package md.lebo.getreal.fragments.awards

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.awards_list_fragment.view.*
import kotlinx.android.synthetic.main.feedback_layout.*
import md.lebo.getreal.R
import md.lebo.getreal.adapters.AwardsAdapter
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class AwardsListFragment : BaseFragment() {
    private lateinit var awardsResponse: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private lateinit var mHud: KProgressHUD
    private val compositeDisposable = CompositeDisposable()
    private lateinit var awardsLayout: View

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        awardsLayout = inflater!!.inflate(R.layout.awards_list_fragment, container, false)
        setHasOptionsMenu(true)
        awardsLayout.awardsRecycler.setHasFixedSize(true)
        awardsLayout.awardsRecycler.layoutManager = LinearLayoutManager(
                activity,
                LinearLayoutManager.VERTICAL,
                false
        )
        mHud = Utils.createProgressHud(activity)
        getAwardsList()
        return awardsLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.awards_title))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.clear()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        inflater?.inflate(R.menu.create_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.createOption -> {
                Utils.loadFragmentWithBackStack(
                        R.id.mainFragmentContainer,
                        AwardCreateFragment(),
                        activity.fragmentManager)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getAwardsList() {
        mHud.show()
        awardsResponse = grApiService.getAwards(
                GRPrefs.apiToken, GRPrefs.userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result != null) {
                        if (result.awards.isEmpty()) {
                            if (mHud.isShowing) mHud.dismiss()
                            Utils.feedbackMessage(
                                    feedbackSnackbar,
                                    "No awards found!"
                            ) {}
                        } else {
                            awardsLayout.awardsRecycler.adapter = AwardsAdapter(result)
                            if (mHud.isShowing) mHud.dismiss()
                        }
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
        compositeDisposable.add(awardsResponse)
    }
}