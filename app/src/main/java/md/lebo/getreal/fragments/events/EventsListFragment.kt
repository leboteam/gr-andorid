package md.lebo.getreal.fragments.events

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.events_list_fragment.*
import kotlinx.android.synthetic.main.events_list_fragment.view.*
import kotlinx.android.synthetic.main.feedback_layout.*
import md.lebo.getreal.R
import md.lebo.getreal.adapters.EventsAdapter
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.models.events.Event
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class EventsListFragment : BaseFragment() {
    private lateinit var eventsDisposable: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private lateinit var mHud: KProgressHUD
    private val events: ArrayList<Event> = ArrayList()
    private lateinit var mLinearLayoutManager: LinearLayoutManager
    private lateinit var mEventsAdapter: EventsAdapter

    //pagination
    private val PAGE_SIZE = 5
    private var isLastPage = false
    private var mCurrentPage = 1
    private var isLoading = false

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        val eventsListLayout = inflater!!.inflate(R.layout.events_list_fragment, container, false)
        setHasOptionsMenu(true)

        mHud = Utils.createProgressHud(this@EventsListFragment.activity)

        if (container != null) {
            mLinearLayoutManager = LinearLayoutManager(
                    activity,
                    LinearLayoutManager.VERTICAL,
                    false)

            eventsListLayout.eventsRecycler.setHasFixedSize(true)
            eventsListLayout.eventsRecycler.layoutManager = mLinearLayoutManager
            eventsListLayout.eventsRecycler.addOnScrollListener(mRecyclerScrollListener)
        }

        val args = arguments
        println(args)
        if (args != null
                && args.getString("EVENT_TITLE", "").isNotEmpty()
                && args.getString("EVENT_START", "").isNotEmpty()
                && args.getString("EVENT_END", "").isNotEmpty()) {
            getEvents(args)
        } else if (args != null && args.getBoolean("EVENTS_MINE", false)) {
            getEvents(args)
        } else {
            getEvents()
        }
        return eventsListLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.events_title))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu!!.clear()
        inflater!!.inflate(R.menu.create_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.createOption -> if (GRPrefs.userId > 0 && GRPrefs.apiToken.isNotEmpty()) {
                Utils.loadFragmentWithBackStack(
                        R.id.mainFragmentContainer,
                        EventCreateFragment(),
                        activity.fragmentManager
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (events.isNotEmpty()) {
            mCurrentPage = 1
            isLoading = false
            events.clear()
            mEventsAdapter.notifyDataSetChanged()
        }
        compositeDisposable.clear()
    }

    private fun onEventClicked(event: Event) {
        val bundle = Bundle()
        bundle.putInt("EVENT_ID", event.id)
        bundle.putInt("EVENT_AUTHOR_ID", event.author.id)
        val fragment = EventDetailsFragment()
        fragment.arguments = bundle
        Utils.loadFragmentWithBackStack(
                R.id.mainFragmentContainer,
                fragment,
                activity.fragmentManager
        )
    }

    private fun getEvents(args: Bundle? = null) {
        mHud.show()
        val optionsMap = HashMap<String, String>()
        optionsMap["page"] = mCurrentPage.toString()
        optionsMap["length"] = PAGE_SIZE.toString()
        if (args != null
                && args.getString("EVENT_TITLE", "").isNotEmpty()
                && args.getString("EVENT_START", "").isNotEmpty()
                && args.getString("EVENT_END", "").isNotEmpty()) {
            optionsMap["title"] = args.getString("EVENT_TITLE")
            optionsMap["dateFrom"] = args.getString("EVENT_START")
            optionsMap["dateTo"] = args.getString("EVENT_END")
        }
        if (args != null && args.getBoolean("EVENTS_MINE") && GRPrefs.userId > 0) {
            optionsMap["user"] = GRPrefs.userId.toString()
        }
        eventsDisposable = grApiService.getEventsList(optionsMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    println("RESULT: $result")
                    isLoading = false
                    if (result != null) {
                        if (events.isEmpty() && result.events.isEmpty()) {
                            if (mHud.isShowing) mHud.dismiss()
                            Utils.feedbackMessage(
                                    feedbackSnackbar,
                                    "No events found!"
                            ) { this@EventsListFragment.activity.fragmentManager.popBackStack() }
                        } else {
                            val items = result.events
                            if (events.isEmpty()) {
                                events.addAll(result.events)
                                mEventsAdapter = EventsAdapter(events) { event: Event -> onEventClicked(event) }
                                eventsRecycler.adapter = mEventsAdapter
                                if (mHud.isShowing) mHud.dismiss()
                            } else {
                                if (items.isNotEmpty()) mEventsAdapter.addEvents(items)
                                if (items.size >= PAGE_SIZE) {
                                    if (mHud.isShowing) mHud.dismiss()
                                } else {
                                    if (mHud.isShowing) mHud.dismiss()
                                    isLastPage = true
                                    mCurrentPage = 1
                                }
                            }
                        }
                    }
                }, { error ->
                    println("EVENTS: $error")
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
        compositeDisposable.add(eventsDisposable)
    }

    private val mRecyclerScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val visibleItemCount = mLinearLayoutManager.childCount
            val totalItemCount = mLinearLayoutManager.itemCount
            val firstVisibleItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition()

            if (!isLoading && !isLastPage) {
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    loadMoreItems()
                }
            }
        }
    }

    private fun loadMoreItems() {
        isLoading = true
        mCurrentPage += 1
        getEvents()
    }
}