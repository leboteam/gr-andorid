package md.lebo.getreal.fragments.clubs

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.*
import com.ablanco.imageprovider.ImageProvider
import com.ablanco.imageprovider.ImageSource
import com.bumptech.glide.Glide
import com.erikagtierrez.multiple_media_picker.Gallery
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.club_create_fragment.view.*
import kotlinx.android.synthetic.main.feedback_layout.*
import kotlinx.android.synthetic.main.map_dialog_layout.*
import md.lebo.getreal.R
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.*

class ClubCreateFragment : BaseFragment() {
    private lateinit var clubResponse: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private lateinit var mHud: KProgressHUD
    private lateinit var clubCreateLayout: View
    private var imageSelected: Bitmap? = null
    private var locationValue: LatLng? = null
    val OPEN_MEDIA_PICKER = 1536
    val READ_STORAGE_PERMISSION_REQUEST_CODE = 1537

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        clubCreateLayout = inflater!!.inflate(R.layout.club_create_fragment, container, false)
        mHud = Utils.createProgressHud(this@ClubCreateFragment.activity)

        clubCreateLayout.clubCreateTakePhotoBtn.setOnClickListener {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(
                                Manifest.permission.CAMERA),
                        Utils.CAMERA_PERMISSION_CODE)
            } else {
                ImageProvider(activity).getImage(ImageSource.CAMERA) { bitmap ->
                    clubCreateLayout.clubCreateBackground.setImageBitmap(bitmap)
                    imageSelected = bitmap
                }
            }
        }

        clubCreateLayout.clubCreateSelectPhotoBtn.setOnClickListener {
            if (checkPermissionForReadExternalStorage()) {
                val intent = Intent(activity, Gallery::class.java)
                intent.putExtra("title", "Média")
                intent.putExtra("mode", 2)
                intent.putExtra("maxSelection", 1)
                startActivityForResult(intent, OPEN_MEDIA_PICKER)
            } else {
                requestPermissionForReadExternalStorage()
            }
        }

        clubCreateLayout.clubCreateShowMapBtn.setOnClickListener {
            println("LOCAL $locationValue")
            showMapDialog(activity)
        }

        if (
                arguments != null &&
                arguments.getBoolean("EDIT_CLUB", false) &&
                arguments.getInt("CLUB_ID_EDIT", 0) > 0) {
            getClub(arguments.getInt("CLUB_ID_EDIT"))
        }
        return clubCreateLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.create_club_title))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        inflater?.inflate(R.menu.add_menu, menu)
    }

    private fun checkPermissionForReadExternalStorage(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val result = activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            return result == PackageManager.PERMISSION_GRANTED
        }
        return false
    }

    @Throws(Exception::class)
    private fun requestPermissionForReadExternalStorage() {
        try {
            ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    READ_STORAGE_PERMISSION_REQUEST_CODE)
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.addOption -> if (
                    clubCreateLayout.clubCreateTitle.text.isNotEmpty() &&
                    clubCreateLayout.clubCreateDescription.text.toString().isNotEmpty() &&
                    clubCreateLayout.clubCreatePrice.text.toString().isNotEmpty() &&
                    clubCreateLayout.clubCreateWebSite.text.toString().isNotEmpty() &&
                    clubCreateLayout.clubCreateTime.text.toString().isNotEmpty() &&
                    clubCreateLayout.clubCreateAddressInput.text.toString().isNotEmpty() &&
                    locationValue != null &&
                    imageSelected != null
            ) {
                val body = Utils.getMultiPartData(Utils.convertBitmapToJpeg(
                        imageSelected,
                        this@ClubCreateFragment.activity,
                        "GET_REAL_IMAGE"), "image")
                val data = HashMap<String, RequestBody>()
                val location = locationValue!!
                data["title"] = RequestBody.create(MediaType.parse("text/plain"), clubCreateLayout.clubCreateTitle.text.toString())
                data["lat"] = RequestBody.create(MediaType.parse("text/plain"), location.latitude.toString())
                data["lng"] = RequestBody.create(MediaType.parse("text/plain"), location.longitude.toString())
                data["description"] = RequestBody.create(MediaType.parse("text/plain"), clubCreateLayout.clubCreateDescription.text.toString())
                data["price"] = RequestBody.create(MediaType.parse("text/plain"), clubCreateLayout.clubCreatePrice.text.toString())
                data["openingHours"] = RequestBody.create(MediaType.parse("text/plain"), clubCreateLayout.clubCreateTime.text.toString())
                data["website"] = RequestBody.create(MediaType.parse("text/plain"), clubCreateLayout.clubCreateWebSite.text.toString())
                data["address"] = RequestBody.create(MediaType.parse("text/plain"), clubCreateLayout.clubCreateAddressInput.text.toString())
                if (
                        arguments != null &&
                        arguments.getBoolean("EDIT_CLUB", false) &&
                        arguments.getInt("CLUB_ID_EDIT", 0) > 0) {
                    editClub(arguments.getInt("CLUB_ID_EDIT", 0), data, body)
                } else {
                    createClub(data, body)
                }
            } else {
                Utils.feedbackMessage(
                        feedbackSnackbar,
                        "All fields must be completed!"
                ) {}
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun createClub(
            clubHashMap: HashMap<String, RequestBody>,
            body: MultipartBody.Part
    ) {
        mHud.show()
        clubResponse = grApiService.createClub(
                GRPrefs.apiToken,
                clubHashMap,
                body
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    println("RESULT: $result")
                    if (mHud.isShowing) mHud.dismiss()
                    activity.fragmentManager.popBackStack()
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    clubCreateLayout.scrollTo(0, clubCreateLayout.bottom)
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    private fun editClub(
            clubID: Int,
            clubHashMap: HashMap<String, RequestBody>,
            body: MultipartBody.Part
    ) {
        mHud.show()
        clubResponse = grApiService.editClub(
                GRPrefs.apiToken,
                clubID,
                clubHashMap,
                body
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    println("RESULT: $result")
                    if (mHud.isShowing) mHud.dismiss()
                    activity.fragmentManager.popBackStack()
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    clubCreateLayout.scrollTo(0, clubCreateLayout.bottom)
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    private fun getClub(clubID: Int) {
        mHud.show()
        clubResponse = grApiService.getClub(GRPrefs.apiToken, clubID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    println("CLUB RESULT: $result")
                    clubCreateLayout.clubCreateTitle.setText(result.title)
                    clubCreateLayout.clubCreateBackground.setImageURI(result.image)
                    clubCreateLayout.clubCreateDescription.setText(result.description)
                    clubCreateLayout.clubCreatePrice.setText(result.price)
                    clubCreateLayout.clubCreateWebSite.setText(result.website)
                    clubCreateLayout.clubCreateTime.setText(result.openingHours)
                    clubCreateLayout.clubCreateAddressInput.setText(result.address)
                    locationValue = LatLng(result.lat, result.lng)
                    if (mHud.isShowing) mHud.dismiss()
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>?,
            grantResults: IntArray?) {

        when (requestCode) {
            Utils.TAKE_PHOTO_CODE -> {
                val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (takePhotoIntent.resolveActivity(activity.packageManager) != null) {
                    ImageProvider(activity).getImage(ImageSource.CAMERA) { bitmap ->
                        imageSelected = bitmap
                        Glide.with(activity).load(bitmap).into(clubCreateLayout.clubCreateBackground)
                    }
                }
                return
            }
            else -> {
                Utils.feedbackMessage(
                        feedbackSnackbar,
                        "It won't be possible to take photos"
                ) {}
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == OPEN_MEDIA_PICKER) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val selectionResult = data.getStringArrayListExtra("result")
                val imageFile = BitmapFactory.decodeFile(selectionResult[0])
                imageSelected = imageFile
                clubCreateLayout.clubCreateBackground.setImageBitmap(imageFile)
            }
        }
    }

    private fun showMapDialog(
            context: Activity) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.map_dialog_layout)
        dialog.show()

        MapsInitializer.initialize(context)
        dialog.dialogMap.onCreate(dialog.onSaveInstanceState())
        dialog.dialogMap.onResume()
        dialog.dialogMap.getMapAsync { googleMap ->
            if (locationValue != null) {
                println("LOCAL $locationValue")
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationValue, 16f))
                val oldMarker = MarkerOptions()
                oldMarker.position(locationValue as LatLng)
                googleMap.addMarker(oldMarker)
            }

            googleMap?.setOnMapLongClickListener { latLng ->
                println("LAT_LNG: $latLng")
                googleMap.clear()
                val marker = MarkerOptions()
                marker.position(latLng)
                googleMap.addMarker(marker)
                locationValue = latLng
                val geocoder = Geocoder(activity, Locale.getDefault())
                val addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
                clubCreateLayout.clubCreateAddressInput.setText(String.format(
                        "%s, %s, %s",
                        addresses[0].countryName,
                        addresses[0].locality,
                        addresses[0].getAddressLine(0)
                ))
            }
        }
    }
}