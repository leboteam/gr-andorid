package md.lebo.getreal.fragments.message

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.feedback_layout.*
import kotlinx.android.synthetic.main.messages_list_fragment.*
import kotlinx.android.synthetic.main.messages_list_fragment.view.*
import md.lebo.getreal.R
import md.lebo.getreal.adapters.MessageAdapter
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.fragments.profile.MessagesFragment
import md.lebo.getreal.models.Conversation
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class MessageList : BaseFragment() {
    private lateinit var messagesResponse: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private lateinit var mHud: KProgressHUD
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val messageLayout = inflater!!.inflate(R.layout.messages_list_fragment, container, false)
        mHud = Utils.createProgressHud(activity)
        messageLayout.messagesListRecycler.setHasFixedSize(true)
        messageLayout.messagesListRecycler.layoutManager = LinearLayoutManager(
                activity,
                LinearLayoutManager.VERTICAL,
                false
        )
        getMessageList()
        return messageLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.conversations))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu?.clear()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.clear()
    }

    private fun onMessageClicked(conversation: Conversation) {
        val bundle = Bundle()
        bundle.putInt("CONVERSATION_ID", conversation.id)
        val fragment = MessagesFragment()
        fragment.arguments = bundle
        Utils.loadFragmentWithBackStack(
                R.id.mainFragmentContainer,
                fragment,
                activity.fragmentManager
        )
    }

    private fun getMessageList() {
        mHud.show()
        messagesResponse = grApiService.getMessageList(GRPrefs.apiToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result.conversations.isEmpty()) {
                        if(mHud.isShowing) mHud.dismiss()
                        Utils.feedbackMessage(
                                feedbackSnackbar,
                                "No conversations found"
                        ) { activity.fragmentManager.popBackStack() }
                    } else {
                        if (mHud.isShowing) mHud.dismiss()
                        println("MESSAGES $result")
                        messagesListRecycler.adapter = MessageAdapter(result)
                        { conversation: Conversation -> onMessageClicked(conversation) }
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)) {}
                })
        compositeDisposable.addAll(messagesResponse)
    }
}