package md.lebo.getreal.fragments.comments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.feedback_layout.*
import kotlinx.android.synthetic.main.messages_fragment.*
import kotlinx.android.synthetic.main.messages_fragment.view.*
import md.lebo.getreal.R
import md.lebo.getreal.adapters.CommentsAdapter
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class CommentsFragment : BaseFragment() {
    private lateinit var messagesResponse: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private lateinit var commentsLayout: View
    private lateinit var mHud: KProgressHUD
    private var clubID = 0

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        commentsLayout = inflater!!.inflate(R.layout.messages_fragment, container, false)
        setHasOptionsMenu(true)
        mHud = Utils.createProgressHud(activity)
        commentsLayout.messagesRecyclerView.setHasFixedSize(true)
        commentsLayout.messagesRecyclerView.layoutManager = LinearLayoutManager(
                activity,
                LinearLayoutManager.VERTICAL,
                false
        )
        clubID = arguments.getInt("COMMENTS_CLUB_ID", 0)
        if (clubID > 0) {
            getComments(clubID)
        }

        commentsLayout.postMessage.setOnClickListener {
            if (commentsLayout.messagesMsgInput.text.toString().isNotEmpty()) {
                postComment(clubID, commentsLayout.messagesMsgInput.text.toString())
            }
        }
        return commentsLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle("Commentaries")
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        inflater!!.inflate(R.menu.post_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.postOption -> {
                if (commentsLayout.messagesMsgInput.text.toString().isNotEmpty() && clubID > 0) {
                    postComment(clubID, commentsLayout.messagesMsgInput.text.toString())
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getComments(clubID: Int) {
        mHud.show()
        messagesResponse = grApiService.getComments(GRPrefs.apiToken, clubID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result != null && result.comments.isNotEmpty()) {
                        if (mHud.isShowing) mHud.dismiss()
                        println("comments $result")
                        messagesProfileImage.setImageURI(result.comments[0].author.image)
                        commentsLayout.messagesRecyclerView.adapter = CommentsAdapter(result)
                    } else {
                        if (mHud.isShowing) mHud.dismiss()
                        Utils.feedbackMessage(
                                feedbackSnackbar,
                                "No comments found"
                        ) {}
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    println("error $error")
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)) {}
                })
        compositeDisposable.addAll(messagesResponse)
    }

    private fun postComment(clubID: Int, message: String) {
        mHud.show()
        messagesResponse = grApiService.postComment(
                GRPrefs.apiToken,
                clubID,
                message)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result != null) {
                        commentsLayout.messagesMsgInput.text.clear()
                        if (mHud.isShowing) mHud.dismiss()
                        getComments(clubID)
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }
}