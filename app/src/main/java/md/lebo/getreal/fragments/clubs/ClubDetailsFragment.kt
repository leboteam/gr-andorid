package md.lebo.getreal.fragments.clubs

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.author_contact_layout.view.*
import kotlinx.android.synthetic.main.club_details_fragment.view.*
import kotlinx.android.synthetic.main.edit_club_popup.view.*
import kotlinx.android.synthetic.main.feedback_layout.*
import kotlinx.android.synthetic.main.map_dialog_layout.*
import md.lebo.getreal.R
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.fragments.comments.CommentsFragment
import md.lebo.getreal.fragments.events.EventCreateFragment
import md.lebo.getreal.fragments.media.MediaFragment
import md.lebo.getreal.fragments.profile.ViewProfileFragment
import md.lebo.getreal.models.clubs.Club
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class ClubDetailsFragment : BaseFragment() {
    private lateinit var clubLayout: View
    private lateinit var mHud: KProgressHUD
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private lateinit var clubResponse: Disposable
    private var clubAuthorId: Int = 0
    private var clubID: Int = 0
    private var locationValue: LatLng? = null
    private lateinit var currentClub: Club

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        clubLayout = inflater!!.inflate(R.layout.club_details_fragment, container, false)
        setHasOptionsMenu(true)
        mHud = Utils.createProgressHud(this@ClubDetailsFragment.activity)
        clubID = arguments!!.getInt("CLUB_ID", 0)
        if (clubID > 0) {
            getClub(clubID)
        }
        clubLayout.clubDetailsAuthorImage.setOnClickListener {
            if (GRPrefs.userId > 0 && GRPrefs.apiToken.isNotEmpty()) {
                if (clubAuthorId > 0 && clubAuthorId != GRPrefs.userId) {
                    val bundle = Bundle()
                    bundle.putInt("USER_ID", clubAuthorId)
                    val fragment = ViewProfileFragment()
                    fragment.arguments = bundle
                    Utils.loadFragmentWithBackStack(
                            R.id.mainFragmentContainer,
                            fragment,
                            this@ClubDetailsFragment.activity.fragmentManager
                    )
                }
            }
        }

        clubLayout.clubDetailsMapBtn.setOnClickListener {
            showMapDialog(activity)
        }

        clubLayout.clubDetailsMediaBtn.setOnClickListener {
            if (GRPrefs.userId > 0 && GRPrefs.apiToken.isNotEmpty()) {
                val bundle = Bundle()
                bundle.putInt("CLUB_MEDIA_ID", clubID)
                val fragment = MediaFragment()
                fragment.arguments = bundle
                Utils.loadFragmentWithBackStack(
                        R.id.mainFragmentContainer,
                        fragment,
                        fragmentManager
                )
            }
        }

        clubLayout.clubDetailsCommentBtn.setOnClickListener {
            if (GRPrefs.userId > 0 && GRPrefs.apiToken.isNotEmpty()) {
                val bundle = Bundle()
                bundle.putInt("COMMENTS_CLUB_ID", clubID)
                val fragment = CommentsFragment()
                fragment.arguments = bundle
                Utils.loadFragmentWithBackStack(
                        R.id.mainFragmentContainer,
                        fragment,
                        fragmentManager
                )
            }
        }
        return clubLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.clubs_list_title))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        locationValue = null
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        if (arguments.getBoolean("IS_MINE", false)) {
            menu?.clear()
            inflater?.inflate(R.menu.edit_menu, menu)
        } else {
            menu?.clear()
            inflater?.inflate(R.menu.join_club_menu, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.editOption -> if (GRPrefs.userId > 0 && GRPrefs.apiToken.isNotEmpty()) {
                showEditPopupWindow()
            }
            R.id.joinClubOption -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getClub(clubID: Int) {
        mHud.show()
        clubResponse = grApiService.getClub(GRPrefs.apiToken, clubID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    println("CLUB RESULT: $result")
                    currentClub = result
                    clubAuthorId = result.author.id
                    clubLayout.clubDetailsName.text = result.title
                    clubLayout.clubDetailsBackground.setImageURI(result.image)
                    clubLayout.clubDetailsAuthorImage.setImageURI(result.author.image)
                    clubLayout.clubsDetailsOrgValue.text = result.author.firstName
                    clubLayout.clubDetailsFeeValue.text = "${result.price} €"
                    clubLayout.clubDetailsSiteValue.text = result.website
                    clubLayout.clubDetailsHoursValue.text = result.openingHours
                    clubLayout.clubDetailsDescriptionValue.text = result.description
                    clubLayout.clubDetailsAddressValue.text = result.address
                    locationValue = LatLng(result.lat, result.lng)
                    if (mHud.isShowing) mHud.dismiss()
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    private fun deleteClub(clubID: Int, closeWindow: () -> Unit) {
        clubResponse = grApiService.deleteClub(
                GRPrefs.apiToken,
                clubID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (
                            result != null &&
                            result.getAsJsonPrimitive("message")
                                    .asString
                                    .isNotEmpty()) {
                        closeWindow()
                        activity.fragmentManager.popBackStack()
                    }
                }, { error ->
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    private fun showEditPopupWindow() {
        val inflater = LayoutInflater.from(this@ClubDetailsFragment.activity)
        val editClubLayout = inflater.inflate(R.layout.edit_club_popup, null)
        val alertBuilder = AlertDialog.Builder(this@ClubDetailsFragment.activity).create()
        alertBuilder.setView(editClubLayout)
        //edit option
        editClubLayout.editOption.setOnClickListener {
            alertBuilder.dismiss()
            val bundle = Bundle()
            bundle.putBoolean("EDIT_CLUB", true)
            bundle.putInt("CLUB_ID_EDIT", clubID)
            val fragment = ClubCreateFragment()
            fragment.arguments = bundle
            Utils.loadFragmentWithBackStack(
                    R.id.mainFragmentContainer,
                    fragment,
                    this@ClubDetailsFragment.activity.fragmentManager
            )
        }

        //program create option
        editClubLayout.editCreateProgram.setOnClickListener {
            alertBuilder.dismiss()
            val bundle = Bundle()
            bundle.putBoolean("IS_PROGRAM", true)
            val fragment = EventCreateFragment()
            fragment.arguments = bundle
            Utils.loadFragmentWithBackStack(
                    R.id.mainFragmentContainer,
                    fragment,
                    this@ClubDetailsFragment.activity.fragmentManager
            )
        }

        editClubLayout.editCreateCompetition.setOnClickListener {
            alertBuilder.dismiss()
            val bundle = Bundle()
            bundle.putBoolean("IS_COMPETITION", true)
            val fragment = EventCreateFragment()
            fragment.arguments = bundle
            Utils.loadFragmentWithBackStack(
                    R.id.mainFragmentContainer,
                    fragment,
                    this@ClubDetailsFragment.activity.fragmentManager
            )
        }

        editClubLayout.editDeleteOption.setOnClickListener {
            if (clubID > 0) {
                deleteClub(clubID) {
                    alertBuilder.dismiss()
                }
            }
        }

        editClubLayout.cancelClubOption.setOnClickListener {
            alertBuilder.dismiss()
        }

        alertBuilder.setCancelable(true)
        alertBuilder.show()
    }

    private fun showAuthorDialog(context: Activity) {
        val inflater = LayoutInflater.from(this@ClubDetailsFragment.activity)
        val contactAuthorLayout = inflater.inflate(R.layout.author_contact_layout, null)
        val alertBuilder = AlertDialog.Builder(this@ClubDetailsFragment.activity).create()
        alertBuilder.setView(contactAuthorLayout)

        contactAuthorLayout.authorName.text = currentClub.author.firstName

        contactAuthorLayout.viewProfileOption.setOnClickListener {
            if (clubAuthorId > 0) {
                val bundle = Bundle()
                bundle.putInt("USER_ID", clubAuthorId)
                val fragment = ViewProfileFragment()
                fragment.arguments = bundle
                Utils.loadFragmentWithBackStack(
                        R.id.mainFragmentContainer,
                        fragment,
                        this@ClubDetailsFragment.activity.fragmentManager
                )
            }
        }

        contactAuthorLayout.messageAuthor.setOnClickListener {

        }
    }

    private fun showMapDialog(
            context: Activity) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.map_dialog_layout)
        dialog.show()

        MapsInitializer.initialize(context)
        dialog.dialogMap.onCreate(dialog.onSaveInstanceState())
        dialog.dialogMap.onResume()
        dialog.dialogMap.getMapAsync { googleMap ->
            if (locationValue != null) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationValue, 15f))
                val oldMarker = MarkerOptions()
                oldMarker.position(locationValue as LatLng)
                googleMap.addMarker(oldMarker)
            }
        }
    }
}