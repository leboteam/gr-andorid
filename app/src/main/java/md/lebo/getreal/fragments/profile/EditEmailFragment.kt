package md.lebo.getreal.fragments.profile

import android.os.Bundle
import android.view.*
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.edit_email_fragment.view.*
import kotlinx.android.synthetic.main.feedback_layout.*
import md.lebo.getreal.R
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class EditEmailFragment : BaseFragment() {

    private lateinit var emailEditResponse: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private lateinit var mHud: KProgressHUD
    private lateinit var emailEditView: View

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        emailEditView = inflater!!.inflate(R.layout.edit_email_fragment, container, false)
        mHud = Utils.createProgressHud(this@EditEmailFragment.activity)
        setHasOptionsMenu(true)
        return emailEditView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.edit_email_title))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu!!.clear()
        inflater?.inflate(R.menu.add_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.addOption ->
                if (emailEditView.profileEditEmailInput.text.toString().isNotEmpty() &&
                        emailEditView.profileEditEmailPassInput.text.toString().isNotEmpty()) {
                    editEmail(emailEditView.profileEditEmailInput.text.toString(),
                            emailEditView.profileEditEmailPassInput.text.toString())
                }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun editEmail(
            newEmail: String,
            currentPassword: String) {
        mHud.show()
        emailEditResponse = grApiService.changeEmail(
                GRPrefs.apiToken,
                newEmail,
                currentPassword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            result.getAsJsonPrimitive("message").asString
                    ) { this@EditEmailFragment.activity.fragmentManager.popBackStack() }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }
}