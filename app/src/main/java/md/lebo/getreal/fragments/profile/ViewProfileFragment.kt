package md.lebo.getreal.fragments.profile

import android.content.Intent
import android.os.Bundle
import android.view.*
import com.google.gson.JsonNull
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.feedback_layout.*
import kotlinx.android.synthetic.main.view_profile_fragment.*
import md.lebo.getreal.R
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.fragments.message.MessageActivity
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class ViewProfileFragment : BaseFragment() {
    private lateinit var userResponse: Disposable
    private val grApiService: GRAPIService by lazy {
        GRAPIService.create()
    }
    private lateinit var mHud: KProgressHUD
    private lateinit var mFirstName: String
    private var mUserID: Int = 0

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        mHud = Utils.createProgressHud(this@ViewProfileFragment.activity)
        setHasOptionsMenu(true)

        if (arguments != null) {
            val userID: Int? = arguments.getInt("USER_ID", 0)
            if (userID != null && userID > 0) {
                mUserID = userID
                getUserAccount(userID)
            } else {
                getUserAccount()
            }
        } else {
            getUserAccount()
        }
        return inflater!!.inflate(R.layout.view_profile_fragment, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.profile_title))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        if (arguments != null && arguments.getInt("USER_ID", 0) > 0) {
            menu?.clear()
            inflater?.inflate(R.menu.message_menu, menu)
        } else {
            menu?.clear()
            inflater?.inflate(R.menu.modify_menu, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.modifyOption -> {
                Utils.loadFragmentWithBackStack(
                        R.id.mainFragmentContainer,
                        EditProfileFragment(),
                        this@ViewProfileFragment.fragmentManager
                )
            }
            R.id.messageOption -> {
                val intent = Intent(activity, MessageActivity::class.java)
                intent.putExtra("USER_NAME_MSG", mFirstName)
                intent.putExtra("USER_ID_MSG", mUserID)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getUserAccount(userID: Int = 0) {
        mHud.show()
        userResponse = when (userID > 0) {
            false -> grApiService.getUserProfile(
                    "1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a",
                    GRPrefs.apiToken)
            true -> grApiService.getUserProfileSingle(GRPrefs.apiToken, userID)
        }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (mHud.isShowing) mHud.dismiss()
                    println("USER PROFILE: $result")
                    val user = result.getAsJsonObject("user")
                    profileImage.setImageURI(user.getAsJsonPrimitive("image").asString!!)
                    if (userID > 0) {
                        profileName.text = user.getAsJsonPrimitive("firstName").asString!!
                        mFirstName = user.getAsJsonPrimitive("firstName").asString!!
                    } else {
                        profileName.text = String.format("%s %s",
                                user.getAsJsonPrimitive("firstName").asString!!,
                                user.getAsJsonPrimitive("lastName").asString!!)
                    }
                    if (user.get("rating") is JsonNull) {
                        profileUnderName.text = "Pas encore evalue"
                    } else {
                        profileUnderName.text = ""
                    }
                    profileRegistrationDate.text = user.getAsJsonPrimitive("createdAt").asString!!
                    if (user.get("description") is JsonNull) {
                        profileDescriptionValue.text = ""
                    } else {
                        profileDescriptionValue.text = user.getAsJsonPrimitive("description").asString
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    println("ERROR $error")
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }
}