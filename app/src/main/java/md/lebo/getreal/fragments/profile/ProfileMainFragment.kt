package md.lebo.getreal.fragments.profile

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.*
import kotlinx.android.synthetic.main.edit_profile_layout.view.*
import kotlinx.android.synthetic.main.profile_fragment.*
import kotlinx.android.synthetic.main.profile_fragment.view.*
import md.lebo.getreal.R
import md.lebo.getreal.activities.LoginActivity
import md.lebo.getreal.fragments.awards.AwardsListFragment
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.fragments.clubs.ClubsListFragment
import md.lebo.getreal.fragments.events.EventsListFragment
import md.lebo.getreal.fragments.message.MessageList
import md.lebo.getreal.fragments.notifications.NotificationsFragment
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class ProfileMainFragment : BaseFragment() {
    private lateinit var profileLayout: View
    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        profileLayout = inflater!!.inflate(R.layout.profile_fragment, container, false)
        setHasOptionsMenu(true)
        if (GRPrefs.userId == 0 && GRPrefs.apiToken.isEmpty()) {
            profileLayout.noAccountLayout.visibility = View.VISIBLE
        } else {
            profileLayout.noAccountLayout.visibility = View.GONE
        }
        return profileLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.profile_title))

        viewProfile.setOnClickListener {
            Utils.loadFragmentWithBackStack(
                    R.id.mainFragmentContainer,
                    ViewProfileFragment(),
                    this@ProfileMainFragment.activity.fragmentManager
            )
        }

        viewEdit.setOnClickListener {
            showEditPopupWindow()
        }

        viewEvents.setOnClickListener {
            val bundle = Bundle()
            bundle.putBoolean("EVENTS_MINE", true)
            val fragment = EventsListFragment()
            fragment.arguments = bundle

            Utils.loadFragmentWithBackStack(
                    R.id.mainFragmentContainer,
                    fragment,
                    this@ProfileMainFragment.activity.fragmentManager
            )
        }

        viewClubs.setOnClickListener {
            Utils.loadFragmentWithBackStack(
                    R.id.mainFragmentContainer,
                    ClubsListFragment(),
                    this@ProfileMainFragment.activity.fragmentManager,
                    true
            )
        }

        viewNotifications.setOnClickListener {
            Utils.loadFragmentWithBackStack(
                    R.id.mainFragmentContainer,
                    NotificationsFragment(),
                    this@ProfileMainFragment.activity.fragmentManager
            )
        }

        viewAwards.setOnClickListener {
            Utils.loadFragmentWithBackStack(
                    R.id.mainFragmentContainer,
                    AwardsListFragment(),
                    this@ProfileMainFragment.activity.fragmentManager
            )
        }

        viewMessages.setOnClickListener {
            Utils.loadFragmentWithBackStack(
                    R.id.mainFragmentContainer,
                    MessageList(),
                    this@ProfileMainFragment.activity.fragmentManager
            )
        }

        viewCalendar.setOnClickListener {
            Utils.loadFragmentWithBackStack(
                    R.id.mainFragmentContainer,
                    CalendarFragment(),
                    this@ProfileMainFragment.activity.fragmentManager
            )
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        if (GRPrefs.userId == 0 && GRPrefs.apiToken.isEmpty()) {
            inflater!!.inflate(R.menu.connect_menu, menu)
        } else {
            inflater!!.inflate(R.menu.deconnect_menu, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.connectOption -> {
                startActivity(Intent(
                        activity,
                        LoginActivity::class.java
                ))
                activity.finish()
            }
            R.id.deconnectOption -> {
                showAppExitDialog()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showAppExitDialog() {
        val builder = android.app.AlertDialog.Builder(activity)
        builder.setTitle(getString(R.string.app_name))
        builder.setMessage(getString(R.string.close_app_msg))

        builder.setPositiveButton(getString(R.string.yes_action)) { _, _ ->
            GRPrefs.clear()
            activity.finish()
        }

        builder.setNegativeButton(getString(R.string.no_action)) { _, _ ->
        }

        val dialog = builder.create()
        dialog.show()
    }

    private fun showEditPopupWindow() {
        val inflater = LayoutInflater.from(this@ProfileMainFragment.activity)
        val editProfileLayout = inflater.inflate(R.layout.edit_profile_layout, null)
        val alertBuilder = AlertDialog.Builder(this@ProfileMainFragment.activity).create()
        alertBuilder.setView(editProfileLayout)
        editProfileLayout.editProfileOption.setOnClickListener {
            alertBuilder.dismiss()
            Utils.loadFragmentWithBackStack(
                    R.id.mainFragmentContainer,
                    EditProfileFragment(),
                    this@ProfileMainFragment.activity.fragmentManager
            )
        }

        editProfileLayout.editEmailOption.setOnClickListener {
            alertBuilder.dismiss()
            Utils.loadFragmentWithBackStack(
                    R.id.mainFragmentContainer,
                    EditEmailFragment(),
                    this@ProfileMainFragment.activity.fragmentManager
            )
        }

        editProfileLayout.editPasswordOption.setOnClickListener {
            alertBuilder.dismiss()
            Utils.loadFragmentWithBackStack(
                    R.id.mainFragmentContainer,
                    EditPasswordFragment(),
                    this@ProfileMainFragment.activity.fragmentManager
            )
        }

        editProfileLayout.cancelOption.setOnClickListener {
            alertBuilder.dismiss()
        }

        alertBuilder.setTitle("Modifier")
        alertBuilder.setCancelable(true)
        alertBuilder.show()
    }
}