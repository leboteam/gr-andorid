package md.lebo.getreal.fragments.media

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import com.ablanco.imageprovider.ImageProvider
import com.ablanco.imageprovider.ImageSource
import com.erikagtierrez.multiple_media_picker.Gallery
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.feedback_layout.*
import kotlinx.android.synthetic.main.media_fragment.view.*
import kotlinx.android.synthetic.main.media_popup_layout.view.*
import md.lebo.getreal.R
import md.lebo.getreal.adapters.MediaAdapter
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils
import okhttp3.MultipartBody


class MediaFragment : BaseFragment() {
    val OPEN_MEDIA_PICKER = 1536
    val READ_STORAGE_PERMISSION_REQUEST_CODE = 1537
    private var clubID = 0
    private lateinit var mHud: KProgressHUD
    private lateinit var mediaResponse: Disposable
    private lateinit var postMediaDisposable: Disposable
    private val compositeDisposable = CompositeDisposable()
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private lateinit var mediaLayout: View

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        mediaLayout = inflater!!.inflate(R.layout.media_fragment, container, false)
        mediaLayout.mediaRecyclerView.setHasFixedSize(true)
        mediaLayout.mediaRecyclerView.layoutManager = LinearLayoutManager(
                activity,
                LinearLayoutManager.VERTICAL,
                false
        )
        clubID = arguments!!.getInt("CLUB_MEDIA_ID", 0)
        mHud = Utils.createProgressHud(activity)
        if (clubID > 0) {
            getClubMedia(clubID)
        }
        return mediaLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle("Média")
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        inflater?.inflate(R.menu.create_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.createOption -> showMediaPopup()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == OPEN_MEDIA_PICKER) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val selectionResult = data.getStringArrayListExtra("result")
                val imageFile = BitmapFactory.decodeFile(selectionResult[0])
                val body = Utils.getMultiPartData(Utils.convertBitmapToJpeg(
                        imageFile,
                        this@MediaFragment.activity,
                        "GET_REAL_MEDIA"), "file")
                if (clubID > 0) {
                    postMedia(clubID, body)
                }
            }
        }
    }


    private fun getClubMedia(clubID: Int) {
        mHud.show()
        mediaResponse = grApiService.getClubMedia(
                GRPrefs.apiToken,
                clubID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result != null) {
                        if (mHud.isShowing) mHud.dismiss()
                        mediaLayout.mediaRecyclerView.adapter = MediaAdapter(result) {}
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
        compositeDisposable.addAll(mediaResponse)
    }

    private fun postMedia(clubID: Int, file: MultipartBody.Part) {
        mHud.show()
        postMediaDisposable = grApiService.postMedia(
                GRPrefs.apiToken,
                clubID,
                file)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result != null) {
                        if (mHud.isShowing) mHud.dismiss()
                        getClubMedia(clubID)
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
        compositeDisposable.addAll(postMediaDisposable)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.clear()
    }

    private fun showMediaPopup() {
        val inflater = LayoutInflater.from(activity)
        val mediaLayout = inflater.inflate(R.layout.media_popup_layout, null)
        val alertBuilder = AlertDialog.Builder(activity).create()
        alertBuilder.setView(mediaLayout)

        mediaLayout.takePhotoOption.setOnClickListener {
            alertBuilder.dismiss()
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(
                                Manifest.permission.CAMERA),
                        Utils.CAMERA_PERMISSION_CODE)
            } else {
                ImageProvider(activity).getImage(ImageSource.CAMERA) { bitmap ->
                    val body = Utils.getMultiPartData(Utils.convertBitmapToJpeg(
                            bitmap,
                            this@MediaFragment.activity,
                            "GET_REAL_IMAGE"), "file")
                    if (clubID > 0) {
                        postMedia(clubID, body)
                    }
                }
            }
        }

        mediaLayout.galleryOption.setOnClickListener {
            if (checkPermissionForReadExternalStorage()) {
                alertBuilder.dismiss()
                val intent = Intent(activity, Gallery::class.java)
                intent.putExtra("title", "Média")
                intent.putExtra("mode", 1)
                intent.putExtra("maxSelection", 1)
                startActivityForResult(intent, OPEN_MEDIA_PICKER)
            } else {
                requestPermissionForReadExternalStorage()
            }
        }

        mediaLayout.mediaCancelOption.setOnClickListener {
            alertBuilder.dismiss()
        }

        alertBuilder.setTitle("Sélectionnez la soruce de média")
        alertBuilder.setCancelable(true)
        alertBuilder.show()
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>?,
            grantResults: IntArray?) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            READ_STORAGE_PERMISSION_REQUEST_CODE -> {
                if (grantResults!!.isNotEmpty() && (ContextCompat.checkSelfPermission(
                                activity,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE) == grantResults[0])) {
                    showMediaPopup()
                } else {
                    requestPermissionForReadExternalStorage()
                }
            }
            Utils.TAKE_PHOTO_CODE -> {
                val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (takePhotoIntent.resolveActivity(activity.packageManager) != null) {
                    ImageProvider(activity).getImage(ImageSource.CAMERA) { bitmap ->
                        val body = Utils.getMultiPartData(Utils.convertBitmapToJpeg(
                                bitmap,
                                this@MediaFragment.activity,
                                "GET_REAL_IMAGE"), "file")
                        if (clubID > 0) {
                            postMedia(clubID, body)
                        }
                    }
                }
                return
            }
        }
    }

    private fun checkPermissionForReadExternalStorage(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val result = activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            return result == PackageManager.PERMISSION_GRANTED
        }
        return false
    }

    @Throws(Exception::class)
    private fun requestPermissionForReadExternalStorage() {
        try {
            ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    READ_STORAGE_PERMISSION_REQUEST_CODE)
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }

    }
}