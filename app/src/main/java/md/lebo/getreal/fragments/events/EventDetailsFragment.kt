package md.lebo.getreal.fragments.events

import android.os.Bundle
import android.view.*
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.event_details_fragment.view.*
import kotlinx.android.synthetic.main.feedback_layout.*
import md.lebo.getreal.R
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class EventDetailsFragment : BaseFragment() {
    private lateinit var mHud: KProgressHUD
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private lateinit var eventResponse: Disposable
    private lateinit var eventLayout: View
    private var eventAuthorId: Int = 0
    private var eventId: Int = 0
    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        eventLayout = inflater!!.inflate(R.layout.event_details_fragment, container, false)
        setHasOptionsMenu(true)
        mHud = Utils.createProgressHud(this@EventDetailsFragment.activity)
        eventId = arguments.getInt("EVENT_ID", 0)
        eventAuthorId = arguments.getInt("EVENT_AUTHOR_ID", 0)
        if (eventId > 0) {
            getEvent(eventId)
        }
        return eventLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.events_title))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        if (eventAuthorId > 0 && eventAuthorId != GRPrefs.userId) {
            menu?.clear()
        } else {
            menu?.clear()
            inflater?.inflate(R.menu.edit_menu, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.editOption -> if (GRPrefs.userId > 0 && GRPrefs.apiToken.isNotEmpty()) {
                val bundle = Bundle()
                bundle.putBoolean("IS_EDIT_EVENT", true)
                bundle.putInt("EVENT_EDIT_ID", eventId)
                val fragment = EventCreateFragment()
                fragment.arguments = bundle
                Utils.loadFragmentWithBackStack(
                        R.id.mainFragmentContainer,
                        fragment,
                        activity.fragmentManager
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getEvent(eventID: Int) {
        mHud.show()
        eventResponse = grApiService.getEvent(eventID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    println("EVENT RESULT: $result")
                    eventId = result.id
                    eventLayout.eventDetailsName.text = result.title
                    eventLayout.eventDetailsAuthorImage.setImageURI(result.author.image)
                    eventLayout.eventDetailsBackground.setImageURI(result.image)
                    eventLayout.eventsDetailsOrgValue.text = result.author.firstName
                    eventLayout.eventDetailsDateValue.text = result.startsAt
                    eventLayout.eventDetailsTimeValue.text = result.duration
                    eventLayout.eventsDetailsPlaceValue.text = result.places.toString()
                    eventLayout.eventDetailsFeeValue.text = "${result.price} €"
                    eventLayout.eventDetailsDescriptionValue.text = result.description
                    if (mHud.isShowing) mHud.dismiss()
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }
}