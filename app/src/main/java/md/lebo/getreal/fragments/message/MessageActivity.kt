package md.lebo.getreal.fragments.message

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.feedback_layout.*
import kotlinx.android.synthetic.main.message_layout.*
import md.lebo.getreal.R
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class MessageActivity : AppCompatActivity() {
    private lateinit var messageResponse: Disposable
    private val grApiService: GRAPIService by lazy {
        GRAPIService.create()
    }
    private var userID: Int = 0
    private lateinit var mHud: KProgressHUD
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.message_layout)
        mHud = Utils.createProgressHud(this)
        if (intent.extras != null) {
            authorMsgName.text = "Envoyer un message à ${intent.extras.getString("USER_NAME_MSG")}"
            userID = intent.extras.getInt("USER_ID_MSG")
        }

        sendAuthorMessage.setOnClickListener {
            if (authorMsgInput.text.toString().isNotEmpty()) {
                messageUser(
                        userID,
                        authorMsgInput.text.toString()
                )
            }
        }

        authorCancelMsg.setOnClickListener {
            finish()
        }
    }

    private fun messageUser(userID: Int, message: String) {
        mHud.show()
        messageResponse = grApiService.messageUser(GRPrefs.apiToken, userID, message)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result != null) {
                        if (mHud.isShowing) mHud.dismiss()
                        Utils.feedbackMessage(
                                feedbackSnackbar,
                                "Your message was sent successfully"
                        ) {}
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }
}