package md.lebo.getreal.fragments.clubs

import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.clubs_fragment.*
import kotlinx.android.synthetic.main.feedback_layout.*
import kotlinx.android.synthetic.main.filter_club_layout.view.*
import md.lebo.getreal.R
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.map.ClubsInfoAdapter
import md.lebo.getreal.models.clubs.Club
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.Utils
import java.util.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.SupportMapFragment
import com.vk.sdk.util.VKJsonHelper.getMap
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider


class ClubsFragment : BaseFragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private lateinit var mGoogleMap: GoogleMap
    private lateinit var mTitle: String

    private lateinit var clubsDisposable: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private var isClubsListEmpty: Boolean = false
    private val clubs: ArrayList<Club> = ArrayList()
    private val compositeDisposable = CompositeDisposable()

    //pagination
    private val PAGE_SIZE = 10
    private var isLastPage = false
    private var mCurrentPage = 1
    private var isLoading = false

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        val clubsLayout = inflater!!.inflate(R.layout.clubs_fragment, container, false)
        setHasOptionsMenu(true)
        return clubsLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clubsMap.onCreate(savedInstanceState)
        clubsMap.getMapAsync(this@ClubsFragment)
        setActionBarTitle(getString(R.string.clubs_title))
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        MapsInitializer.initialize(this@ClubsFragment.activity)
        if (googleMap != null) {
            mGoogleMap = googleMap
            googleMap.uiSettings.isMyLocationButtonEnabled = true
            googleMap.setOnMarkerClickListener(this@ClubsFragment)
            if ((ContextCompat.checkSelfPermission(
                            activity,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(
                                    activity,
                                    android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                val locationProvider = ReactiveLocationProvider(activity)
                val result = locationProvider.lastKnownLocation
                        .subscribe { location ->
                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    LatLng(location.latitude, location.longitude), 10.1f))
                        }
            } else {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        LatLng(48.8566, 2.3522), 13.1f))
            }
            mGoogleMap.setInfoWindowAdapter(ClubsInfoAdapter(this@ClubsFragment.activity))
            mGoogleMap.setOnInfoWindowClickListener { marker ->
                val obj: Club? = marker?.tag as Club
                if (obj != null) {
                    val bundle = Bundle()
                    bundle.putInt("CLUB_ID", obj.id)
                    val fragment = ClubDetailsFragment()
                    fragment.arguments = bundle
                    Utils.loadFragmentWithBackStack(
                            R.id.mainFragmentContainer,
                            fragment,
                            activity.fragmentManager)
                }
            }
            getClubs()

            if ((ContextCompat.checkSelfPermission(
                            activity,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(
                                    activity,
                                    android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                mGoogleMap.isMyLocationEnabled = true
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        inflater?.inflate(R.menu.map_menu, menu)
        menu!!.findItem(R.id.listOption).isEnabled = !this.isClubsListEmpty
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            when (item.itemId) {
                R.id.filterOption -> showFilterWindow {
                    val bundle = Bundle()
                    bundle.putString("CLUB_TITLE", mTitle)
                    val fragment = ClubsListFragment()
                    fragment.arguments = bundle
                    Utils.loadFragmentWithBackStack(
                            R.id.mainFragmentContainer,
                            fragment,
                            this@ClubsFragment.activity.fragmentManager)
                }
                R.id.listOption -> Utils.loadFragmentWithBackStack(
                        R.id.mainFragmentContainer,
                        ClubsListFragment(),
                        this@ClubsFragment.activity.fragmentManager)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun isLocationServiceEnabled(): Boolean {
        return ((ContextCompat.checkSelfPermission(
                activity,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(
                        activity,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED))

    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        if (marker!!.isInfoWindowShown) marker.hideInfoWindow()
        marker.showInfoWindow()
        return true
    }

    override fun onResume() {
        super.onResume()
        clubsMap.onResume()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        clubsMap.onLowMemory()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        clubsMap.onDestroy()
        compositeDisposable.clear()
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>?,
            grantResults: IntArray?) {

        when (requestCode) {
            Utils.LOCATION_PERMISSION_CODE -> {
                mGoogleMap.isMyLocationEnabled = grantResults!!.isNotEmpty() &&
                        (ContextCompat.checkSelfPermission(
                                activity,
                                android.Manifest.permission.ACCESS_COARSE_LOCATION) == grantResults[0] &&
                                ContextCompat.checkSelfPermission(
                                        activity,
                                        android.Manifest.permission.ACCESS_FINE_LOCATION) == grantResults[1])
                onMapReady(mGoogleMap)
                val locationProvider = ReactiveLocationProvider(activity)
                locationProvider.lastKnownLocation
                        .subscribe { location ->
                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    LatLng(location.latitude, location.longitude), 13.1f))
                        }
                return
            }
            else -> {
                Utils.feedbackMessage(
                        feedbackSnackbar,
                        "My location button will not work."
                ) {}
            }
        }
    }

    private fun showFilterWindow(callbackMethod: () -> Unit) {

        val inflater = LayoutInflater.from(this@ClubsFragment.activity)
        val filterLayout = inflater.inflate(R.layout.filter_club_layout, null)
        val alertDialog = AlertDialog.Builder(this@ClubsFragment.activity).create()
        alertDialog.setView(filterLayout)

        filterLayout.filterClubButton.setOnClickListener {
            if (filterLayout.filterTitleClubInput.text.toString().isNotEmpty()) {
                mTitle = filterLayout.filterTitleClubInput.text.toString()
                callbackMethod()
                alertDialog.dismiss()
            } else {
                filterLayout.filterTitleClubInput.setText("")
            }
        }
        alertDialog.show()
    }

    private fun getClubs(args: Bundle? = null) {
        val optionsMap = HashMap<String, String>()
        optionsMap["page"] = mCurrentPage.toString()
        optionsMap["length"] = PAGE_SIZE.toString()
        clubsDisposable = grApiService.getClubsList(optionsMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    println("RESULT: $result")
                    isLoading = false
                    if (result != null) {
                        if (result.clubs.isEmpty() && clubs.isEmpty()) {
                            this.isClubsListEmpty = true
                            activity.invalidateOptionsMenu()
                            Utils.feedbackMessage(
                                    feedbackSnackbar,
                                    "No clubs found!"
                            ) { this@ClubsFragment.activity.fragmentManager.popBackStack() }
                        } else {
                            val items = result.clubs
                            if (items.isNotEmpty()) {
                                clubs.addAll(result.clubs)
                                for (i in 0 until items.size) {
                                    if (!isLocationServiceEnabled()) {
                                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(items[0].lat, items[0].lng), 13.1f))
                                    }
                                    val markerOpt = MarkerOptions()
                                    markerOpt
                                            .position(LatLng(items[i].lat, items[i].lng))
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                                    val marker = mGoogleMap.addMarker(markerOpt)
                                    marker.tag = items[i]
                                }
                                if (items.size >= PAGE_SIZE) {
                                    println("LOAD_MORE!!!")
                                    loadMoreItems()
                                } else {
                                    isLastPage = true
                                    mCurrentPage = 1
                                }
                            }
                        }
                    }
                }, { error ->
                    println("CLUBS ERROR $error")
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
        compositeDisposable.add(clubsDisposable)
    }

    private fun loadMoreItems() {
        isLoading = true
        mCurrentPage += 1
        getClubs()
    }
}