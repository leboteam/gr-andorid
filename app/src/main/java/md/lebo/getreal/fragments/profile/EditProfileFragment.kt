package md.lebo.getreal.fragments.profile

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.*
import android.widget.Toast
import com.google.gson.JsonNull
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.edit_profile_fragment.view.*
import kotlinx.android.synthetic.main.feedback_layout.*
import md.lebo.getreal.R
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.FileNotFoundException


class EditProfileFragment : BaseFragment() {
    private lateinit var userResponse: Disposable
    private val grApiService: GRAPIService by lazy {
        GRAPIService.create()
    }
    private lateinit var mHud: KProgressHUD
    private lateinit var editProfileView: View
    private lateinit var imageSelected: Bitmap

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        editProfileView = inflater!!.inflate(R.layout.edit_profile_fragment, container, false)
        mHud = Utils.createProgressHud(this@EditProfileFragment.activity)
        setHasOptionsMenu(true)

        editProfileView.profileEditTakePhotoBtn.setOnClickListener {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CAMERA), Utils.CAMERA_PERMISSION_CODE)
            } else {
                val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (takePhotoIntent.resolveActivity(activity.packageManager) != null) {
                    startActivityForResult(takePhotoIntent, Utils.TAKE_PHOTO_CODE)
                }
            }
        }

        editProfileView.profileEditSelectPhoto.setOnClickListener {
            val selectPhoto = Intent()
            selectPhoto.type = "image/*"
            selectPhoto.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(selectPhoto, Utils.SELECT_PHOTO_CODE)
        }
        return editProfileView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.edit_profile_title))
        getUserAccount()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu!!.clear()
        inflater?.inflate(R.menu.add_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.addOption -> {
                val body = Utils.getMultiPartData(Utils.convertBitmapToJpeg(
                        imageSelected,
                        this@EditProfileFragment.activity,
                        "GET_REAL_PROFILE"), "image")
                updateProfile(
                        RequestBody.create(MediaType.parse("text/plain"), editProfileView.profileEditNameInput.text.toString()),
                        RequestBody.create(MediaType.parse("text/plain"), editProfileView.profileEditSurnameInput.text.toString()),
                        body,
                        RequestBody.create(MediaType.parse("text/plain"), editProfileView.profileEditDescription.text.toString())
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>?,
            grantResults: IntArray?) {

        when (requestCode) {
            Utils.TAKE_PHOTO_CODE -> {
                val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (takePhotoIntent.resolveActivity(activity.packageManager) != null) {
                    startActivityForResult(takePhotoIntent, Utils.OPEN_CAMERA_CODE)
                }
                return
            }
            else -> {
                Utils.feedbackMessage(
                        feedbackSnackbar,
                        "It won't be possible to take photos"
                ) {}
            }
        }
    }

    private fun getUserAccount() {
        mHud.show()
        userResponse = grApiService.getUserProfile(
                "1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a",
                GRPrefs.apiToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (mHud.isShowing) mHud.dismiss()
                    println("USER PROFILE: $result")
                    val user = result.getAsJsonObject("user")
                    editProfileView.profileEditSurnameInput.setText(user.getAsJsonPrimitive("lastName").asString)
                    editProfileView.profileEditNameInput.setText(user.getAsJsonPrimitive("firstName").asString)
                    editProfileView.profileEditImage.setImageURI(user.getAsJsonPrimitive("image").asString)
                    if (user.get("description") is JsonNull) {
                        editProfileView.profileEditDescription.setText("")
                    } else {
                        editProfileView.profileEditDescription.setText(user.getAsJsonPrimitive("description").asString)
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    println("ERROR $error")
                    editProfileView.profileScrollView.fullScroll(View.FOCUS_DOWN)
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    private fun updateProfile(firstName: RequestBody,
                              lastName: RequestBody,
                              profileImage: MultipartBody.Part,
                              profileDescription: RequestBody) {
        mHud.show()
        userResponse = grApiService.editProfile(
                GRPrefs.apiToken,
                firstName,
                lastName,
                profileImage,
                profileDescription)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (mHud.isShowing) mHud.dismiss()
                    println("USER PROFILE: $result")
                    val user = result.getAsJsonObject("user")
                    if (user != null) {
                        editProfileView.profileScrollView.scrollTo(0, editProfileView.profileScrollView.bottom)
                        Utils.feedbackMessage(
                                feedbackSnackbar,
                                "Successfully updated"
                        ) { this@EditProfileFragment.activity.fragmentManager.popBackStack() }
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    println("ERROR $error")
                    editProfileView.profileScrollView.fullScroll(View.FOCUS_DOWN)
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Utils.SELECT_PHOTO_CODE && resultCode == RESULT_OK) {
            try {
                val imageUri = data?.data
                val imageStream = activity.contentResolver.openInputStream(imageUri)
                imageSelected = BitmapFactory.decodeStream(imageStream)
                editProfileView.profileEditImage.setImageBitmap(imageSelected)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
                Toast.makeText(this@EditProfileFragment.activity, "Something went wrong", Toast.LENGTH_LONG).show()
            }
        } else if (requestCode == Utils.TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
            val extras = data?.extras
            imageSelected = extras!!.get("data") as Bitmap
            editProfileView.profileEditImage.setImageBitmap(imageSelected)
        } else {
            Toast.makeText(this@EditProfileFragment.activity, "You haven't picked Image", Toast.LENGTH_LONG).show()
        }
    }
}