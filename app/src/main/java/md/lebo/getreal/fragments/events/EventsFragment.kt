package md.lebo.getreal.fragments.events

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.events_fragment.*
import kotlinx.android.synthetic.main.feedback_layout.*
import kotlinx.android.synthetic.main.filter_event_layout.view.*
import md.lebo.getreal.R
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.map.EventsInfoAdapter
import md.lebo.getreal.models.events.Event
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils
import java.util.*
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.Circle
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider
import pl.charmas.android.reactivelocation2.observables.location.LastKnownLocationObservableOnSubscribe


class EventsFragment :
        BaseFragment(),
        OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener,
        DatePickerDialog.OnDateSetListener {
    private lateinit var mGoogleMap: GoogleMap
    private lateinit var mTitle: String
    private lateinit var mStartDate: String
    private lateinit var mEndDate: String

    private lateinit var eventsDisposable: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private var isEventsListEmpty: Boolean = false
    private val events: ArrayList<Event> = ArrayList()
    private val compositeDisposable = CompositeDisposable()

    //pagination
    private val PAGE_SIZE = 10
    private var isLastPage = false
    private var mCurrentPage = 1
    private var isLoading = false

    private val now: Calendar = Calendar.getInstance()

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        val eventsLayout = inflater!!.inflate(R.layout.events_fragment, container, false)
        setHasOptionsMenu(true)
        return eventsLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        eventsMap.onCreate(savedInstanceState)
        eventsMap.getMapAsync(this@EventsFragment)
        setActionBarTitle(getString(R.string.events_title))
        println("TOKEN: ${GRPrefs.apiToken}")
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        MapsInitializer.initialize(this@EventsFragment.activity)
        if (googleMap != null) {
            mGoogleMap = googleMap
            googleMap.uiSettings.isMyLocationButtonEnabled = true
            googleMap.setOnMarkerClickListener(this@EventsFragment)

            if ((ContextCompat.checkSelfPermission(
                            activity,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(
                                    activity,
                                    android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                val locationProvider = ReactiveLocationProvider(activity)
                val result = locationProvider.lastKnownLocation
                        .subscribe { location ->
                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    LatLng(location.latitude, location.longitude), 13.1f))
                        }
            } else {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        LatLng(48.8566, 2.3522), 13.1f))
            }

            mGoogleMap.setInfoWindowAdapter(EventsInfoAdapter(this@EventsFragment.activity))
            mGoogleMap.setOnInfoWindowClickListener { marker ->
                val obj: Event? = marker?.tag as Event
                if (obj != null) {
                    val bundle = Bundle()
                    bundle.putInt("EVENT_ID", obj.id)
                    bundle.putInt("EVENT_AUTHOR_ID", obj.author.id)
                    val fragment = EventDetailsFragment()
                    fragment.arguments = bundle
                    Utils.loadFragmentWithBackStack(
                            R.id.mainFragmentContainer,
                            fragment,
                            activity.fragmentManager
                    )
                }
            }
            getEvents()

            if ((ContextCompat.checkSelfPermission(
                            activity,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(
                                    activity,
                                    android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                mGoogleMap.isMyLocationEnabled = true
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        inflater?.inflate(R.menu.map_menu, menu)
        menu!!.findItem(R.id.filterOption).isEnabled = !this.isEventsListEmpty
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            when (item.itemId) {
                R.id.filterOption -> showFilterWindow {
                    val bundle = Bundle()
                    bundle.putString("EVENT_TITLE", mTitle)
                    bundle.putString("EVENT_START", mStartDate)
                    bundle.putString("EVENT_END", mEndDate)
                    val fragment = EventsListFragment()
                    fragment.arguments = bundle
                    Utils.loadFragmentWithBackStack(
                            R.id.mainFragmentContainer,
                            fragment,
                            this@EventsFragment.activity.fragmentManager)
                }
                R.id.listOption -> {
                    Utils.loadFragmentWithBackStack(
                            R.id.mainFragmentContainer,
                            EventsListFragment(),
                            this@EventsFragment.activity.fragmentManager)
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun isLocationServiceEnabled(): Boolean {
        return ((ContextCompat.checkSelfPermission(
                activity,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(
                        activity,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED))

    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        if (marker!!.isInfoWindowShown) marker.hideInfoWindow()
        marker.showInfoWindow()
        return true
    }

    override fun onResume() {
        super.onResume()
        eventsMap.onResume()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        eventsMap.onLowMemory()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        eventsMap.onDestroy()
        compositeDisposable.clear()
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>?,
            grantResults: IntArray?) {

        when (requestCode) {
            Utils.LOCATION_PERMISSION_CODE -> {
                mGoogleMap.isMyLocationEnabled = grantResults!!.isNotEmpty() &&
                        (ContextCompat.checkSelfPermission(
                                activity,
                                android.Manifest.permission.ACCESS_COARSE_LOCATION) == grantResults[0] &&
                                ContextCompat.checkSelfPermission(
                                        activity,
                                        android.Manifest.permission.ACCESS_FINE_LOCATION) == grantResults[1])
                onMapReady(mGoogleMap)
                val locationProvider = ReactiveLocationProvider(activity)
                locationProvider.lastKnownLocation
                        .subscribe { location ->
                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    LatLng(location.latitude, location.longitude), 10.1f))
                        }
                return
            }
            else -> {
                Utils.feedbackMessage(
                        feedbackSnackbar,
                        "My location button will not work."
                ) {}
            }
        }
    }

    private fun showFilterWindow(callbackMethod: () -> Unit) {

        val inflater = LayoutInflater.from(this@EventsFragment.activity)
        val filterLayout = inflater.inflate(R.layout.filter_event_layout, null)
        val alertDialog = AlertDialog.Builder(this@EventsFragment.activity).create()
        alertDialog.setView(filterLayout)

        filterLayout.filterDateStart.setText(String.format(Locale.US, "%d/%d/%d 00:00",
                now.get(Calendar.DAY_OF_MONTH),
                now.get(Calendar.MONTH),
                now.get(Calendar.YEAR)))
        filterLayout.filterDateStart.isEnabled = false
        filterLayout.filterDateEnd.setOnClickListener {
            val now = Calendar.getInstance()
            val pickerListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                if (filterLayout.filterDateEnd.text.isNotEmpty()) {
                    filterLayout.filterDateEnd.setText("")
                } else {
                    filterLayout.filterDateEnd.setText(String.format(Locale.US, "%d/%d/%d 00:00", dayOfMonth, monthOfYear, year))
                }
            }
            val dpd = DatePickerDialog.newInstance(
                    pickerListener,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            )
            dpd.onDateSetListener = pickerListener
            dpd.show(fragmentManager, "Datepickerdialog")
        }

        filterLayout.filterButton.setOnClickListener {
            if (filterLayout.filterTitleInput.text.toString().isNotEmpty() &&
                    filterLayout.filterDateStart.text.toString().isNotEmpty() &&
                    filterLayout.filterDateEnd.text.toString().isNotEmpty()) {
                mTitle = filterLayout.filterTitleInput.text.toString()
                mStartDate = filterLayout.filterDateStart.text.toString()
                mEndDate = filterLayout.filterDateEnd.text.toString()
                callbackMethod()
                alertDialog.dismiss()
            } else {
                filterLayout.filterTitleInput.setText("")
                filterLayout.filterDateEnd.text = null
            }
        }
        alertDialog.show()
    }

    private fun getEvents() {
        val optionsMap = HashMap<String, String>()
        optionsMap["page"] = mCurrentPage.toString()
        optionsMap["length"] = PAGE_SIZE.toString()
        eventsDisposable = grApiService.getEventsList(optionsMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    println("RESULT: $result")
                    isLoading = false
                    if (result != null) {
                        if (result.events.isEmpty() && events.isEmpty()) {
                            this.isEventsListEmpty = true
                            activity.invalidateOptionsMenu()
                            Utils.feedbackMessage(
                                    feedbackSnackbar,
                                    "No events found!"
                            ) { }
                        } else {
                            val items = result.events
                            if (items.isNotEmpty()) {
                                events.addAll(result.events)
                                for (i in 0 until items.size) {
                                    if (!isLocationServiceEnabled()) {
                                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                                LatLng(items[0].lat, items[0].lng), 13.1f))
                                    }
                                    val markerOpt = MarkerOptions()
                                    markerOpt
                                            .position(LatLng(items[i].lat, items[i].lng))
                                            .title(items[i].title)
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                                    val marker = mGoogleMap.addMarker(markerOpt)
                                    marker.tag = items[i]
                                }
                                if (items.size >= PAGE_SIZE) {
                                    println("LOAD_MORE!!!")
                                    loadMoreItems()
                                } else {
                                    isLastPage = true
                                    mCurrentPage = 1
                                }
                            }
                        }
                    }
                }, { error ->
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
        compositeDisposable.add(eventsDisposable)
    }

    private fun loadMoreItems() {
        isLoading = true
        mCurrentPage += 1
        getEvents()
    }

    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {

    }
}