package md.lebo.getreal.fragments.base

import android.app.Fragment
import android.support.v7.app.AppCompatActivity
import md.lebo.getreal.R
import org.jetbrains.annotations.Nullable

abstract class BaseFragment : Fragment() {
    fun setActionBarTitle(@Nullable title: String) {
        var tempTitle = title
        if (tempTitle.isEmpty()) {
            tempTitle = getString(R.string.app_name)
        }
        val actionBar = (activity as AppCompatActivity).supportActionBar
        if (actionBar != null) {
            actionBar.title = tempTitle
        }
    }
}