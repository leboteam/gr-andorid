package md.lebo.getreal.fragments.profile

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.feedback_layout.*
import kotlinx.android.synthetic.main.messages_fragment.*
import kotlinx.android.synthetic.main.messages_fragment.view.*
import md.lebo.getreal.R
import md.lebo.getreal.adapters.ChatAdapter
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class MessagesFragment : BaseFragment() {
    private lateinit var messagesResponse: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private lateinit var messagesLayout: View
    private lateinit var mHud: KProgressHUD
    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        messagesLayout = inflater!!.inflate(R.layout.messages_fragment, container, false)
        setHasOptionsMenu(true)
        mHud = Utils.createProgressHud(activity)
        messagesLayout.messagesRecyclerView.setHasFixedSize(true)
        messagesLayout.messagesRecyclerView.layoutManager = LinearLayoutManager(
                this@MessagesFragment.activity,
                LinearLayoutManager.VERTICAL,
                false
        )
        val conversationID = arguments.getInt("CONVERSATION_ID", 0)
        if (conversationID > 0) {
            getChatMessages(conversationID)
        }

        messagesLayout.postMessage.setOnClickListener {
            if (messagesLayout.messagesMsgInput.text.toString().isNotEmpty()) {
                postMessgae(conversationID, messagesLayout.messagesMsgInput.text.toString())
            }
        }
        return messagesLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.conversations))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.clear()
    }

    private fun getChatMessages(conversationID: Int) {
        mHud.show()
        messagesResponse = grApiService.getChatMessages(GRPrefs.apiToken, conversationID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (mHud.isShowing) mHud.dismiss()
                    println("MESSAGES $result")
                    messagesProfileImage.setImageURI(result.messages[0].author.image)
                    messagesLayout.messagesRecyclerView.adapter = ChatAdapter(result)
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)) {}
                })
        compositeDisposable.addAll(messagesResponse)
    }

    private fun postMessgae(conversationID: Int, message: String) {
        mHud.show()
        messagesResponse = grApiService.postMessage(
                GRPrefs.apiToken,
                conversationID,
                message)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result != null) {
                        if (mHud.isShowing) mHud.dismiss()
                        getChatMessages(conversationID)
                    }
                }, { error ->
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
    }
}