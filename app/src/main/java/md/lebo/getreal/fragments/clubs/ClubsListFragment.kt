package md.lebo.getreal.fragments.clubs

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.kaopiz.kprogresshud.KProgressHUD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.clubs_list_fragment.*
import kotlinx.android.synthetic.main.clubs_list_fragment.view.*
import kotlinx.android.synthetic.main.feedback_layout.*
import md.lebo.getreal.R
import md.lebo.getreal.adapters.ClubsAdapter
import md.lebo.getreal.fragments.base.BaseFragment
import md.lebo.getreal.models.clubs.Club
import md.lebo.getreal.network.GRAPIService
import md.lebo.getreal.utils.GRPrefs
import md.lebo.getreal.utils.Utils

class ClubsListFragment : BaseFragment() {
    private val clubs: ArrayList<Club> = ArrayList()
    private lateinit var clubsResponse: Disposable
    private val grApiService by lazy {
        GRAPIService.create()
    }
    private lateinit var mHud: KProgressHUD
    private var isMine: Boolean? = false

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private lateinit var mLinearLayoutManager: LinearLayoutManager
    private lateinit var mClubsAdapter: ClubsAdapter

    //pagination
    private val PAGE_SIZE = 5
    private var isLastPage = false
    private var mCurrentPage = 1
    private var isLoading = false

    override fun onCreateView(
            inflater: LayoutInflater?,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View {
        val clubLayout = inflater!!.inflate(R.layout.clubs_list_fragment, container, false)
        setHasOptionsMenu(true)
        mHud = Utils.createProgressHud(this@ClubsListFragment.activity)
        isMine = arguments?.getBoolean("IS_MINE", false)
        if (container != null) {
            mLinearLayoutManager = LinearLayoutManager(
                    activity,
                    LinearLayoutManager.VERTICAL,
                    false
            )
            clubLayout.clubRecycler.setHasFixedSize(true)
            clubLayout.clubRecycler.layoutManager = mLinearLayoutManager
            clubLayout.clubRecycler.addOnScrollListener(mRecyclerScrollListener)
        }
        val args = arguments
        if (args != null
                && args.getString("CLUB_TITLE", "").isNotEmpty()) {
            getClubs(args)
        } else {
            getClubs()
        }
        return clubLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarTitle(getString(R.string.clubs_title))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu!!.clear()
        inflater!!.inflate(R.menu.create_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.createOption -> if (GRPrefs.userId > 0 && GRPrefs.apiToken.isNotEmpty()) {
                Utils.loadFragmentWithBackStack(
                        R.id.mainFragmentContainer,
                        ClubCreateFragment(),
                        this@ClubsListFragment.activity.fragmentManager
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (clubs.isNotEmpty()) {
            clubs.clear()
            mCurrentPage = 1
            isLoading = false
            mClubsAdapter.notifyDataSetChanged()
        }
        compositeDisposable.clear()
    }

    private fun onClubClicked(club: Club) {
        val bundle = Bundle()
        bundle.putInt("CLUB_ID", club.id)
        if (arguments != null && arguments.getBoolean("IS_MINE", false)) {
            bundle.putBoolean("IS_MINE", true)
        }
        val fragment = ClubDetailsFragment()
        fragment.arguments = bundle
        Utils.loadFragmentWithBackStack(
                R.id.mainFragmentContainer,
                fragment,
                this@ClubsListFragment.activity.fragmentManager)
    }

    private fun getClubs(args: Bundle? = null) {
        mHud.show()
        val optionsMap = HashMap<String, String>()
        optionsMap["page"] = mCurrentPage.toString()
        optionsMap["length"] = PAGE_SIZE.toString()
        if (args != null) {
            optionsMap["title"] = args.getString("CLUB_TITLE")
        }
        clubsResponse = when (isMine) {
            null -> grApiService.getClubsList(optionsMap)
            true -> grApiService.getClubsMine(GRPrefs.apiToken, optionsMap)
            false -> grApiService.getClubsList(optionsMap)
        }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    println("RESULT: $result")
                    isLoading = false
                    if (result != null) {
                        if (result.clubs.isEmpty()) {
                            if (mHud.isShowing) mHud.dismiss()
                            Utils.feedbackMessage(
                                    feedbackSnackbar,
                                    "No clubs found!"
                            ) { this@ClubsListFragment.activity.fragmentManager.popBackStack() }
                        } else {
                            val items = result.clubs
                            if (clubs.isEmpty()) {
                                clubs.addAll(result.clubs)
                                mClubsAdapter = ClubsAdapter(clubs) { club: Club -> onClubClicked(club) }
                                clubRecycler.adapter = mClubsAdapter
                                if (mHud.isShowing) mHud.dismiss()
                            } else {
                                if (items.isNotEmpty()) mClubsAdapter.addClubs(items)
                                if (items.size >= PAGE_SIZE) {
                                    if (mHud.isShowing) mHud.dismiss()
                                } else {
                                    if (mHud.isShowing) mHud.dismiss()
                                    isLastPage = true
                                    mCurrentPage = 1
                                }
                            }
                        }
                    }
                }, { error ->
                    println("CLUBS ERROR $error")
                    if (mHud.isShowing) mHud.dismiss()
                    Utils.feedbackMessage(
                            feedbackSnackbar,
                            Utils.requestErrorHandler(error)
                    ) {}
                })
        compositeDisposable.add(clubsResponse)
    }

    private val mRecyclerScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val visibleItemCount = mLinearLayoutManager.childCount
            val totalItemCount = mLinearLayoutManager.itemCount
            val firstVisibleItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition()

            if (!isLoading && !isLastPage) {
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    loadMoreItems()
                }
            }
        }
    }

    private fun loadMoreItems() {
        isLoading = true
        mCurrentPage += 1
        getClubs()
    }
}