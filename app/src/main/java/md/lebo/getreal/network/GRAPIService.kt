package md.lebo.getreal.network

import com.google.gson.JsonObject
import io.reactivex.Flowable
import md.lebo.getreal.models.AwardsData
import md.lebo.getreal.models.MessageData
import md.lebo.getreal.models.NotificationsData
import md.lebo.getreal.models.chat.ChatData
import md.lebo.getreal.models.clubs.Club
import md.lebo.getreal.models.clubs.ClubsData
import md.lebo.getreal.models.comments.CommentsData
import md.lebo.getreal.models.events.Event
import md.lebo.getreal.models.events.EventsData
import md.lebo.getreal.models.media.MediaData
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface GRAPIService {
    companion object {
        fun create(): GRAPIService {
            val okHttpInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
            okHttpInterceptor.level = HttpLoggingInterceptor.Level.BODY
            val okHttp = OkHttpClient.Builder()
            okHttp.addInterceptor(okHttpInterceptor)
            okHttp.retryOnConnectionFailure(true)

            val retrofit = Retrofit.Builder()
//                    .baseUrl("https://getreal.stage.bixel.ro/api/")
//                    .baseUrl("https://getreal.fr/api/")
                    .baseUrl("https://getreal.stage.bixel.ro/app_dev_testflight.php/api/")
                    .client(okHttp.build())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return retrofit.create(GRAPIService::class.java)
        }
    }

    //Test API
    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @GET("hello")
    fun testApi(): Flowable<JsonObject>

    //REGISTER & LOGIN
    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @FormUrlEncoded
    @POST("account/register")
    fun registerUser(@Field("email") userEmail: String,
                     @Field("firstName") firstName: String,
                     @Field("lastName") lastName: String,
                     @Field("plainPassword[first]") plainPassword: String,
                     @Field("plainPassword[second]") plainPasswordRepeat: String): Flowable<JsonObject>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @FormUrlEncoded
    @POST("account/login")
    fun loginUser(@Field("email") userEmail: String,
                  @Field("password") plainPassword: String): Flowable<JsonObject>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @Multipart
    @POST("account/connect/{resource}")
    fun connectViaService(@Path("resource") service: String,
                          @PartMap options: HashMap<String, RequestBody>): Flowable<JsonObject>

    @Multipart
    @POST
    fun getGoogleAccessToken(@Url urlString: String = "https://www.googleapis.com/oauth2/v4/token",
                             @PartMap options: HashMap<String, RequestBody>): Flowable<JsonObject>

    @POST
    fun getTwitterSignature(@Url urlString: String = "https://api.twitter.com/oauth/request_token",
                            @HeaderMap headerOptions: HashMap<String, String>): Flowable<JsonObject>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @GET("account/confirm/{token}")
    fun confirmAccount(@Path("token") token: String): Flowable<JsonObject>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @FormUrlEncoded
    @POST("account/my-profile/email/confirm")
    fun confirmEmailChange(@Header("x-api-token") token: String,
                           @Field("confirmationToken") confirmationToken: String,
                           @Field("target") target: String): Flowable<JsonObject>

    //USER PROFILE
    @GET("/api/account/my-profile")
    fun getUserProfile(
            @Header("x-api-key") key: String,
            @Header("x-api-token") token: String): Flowable<JsonObject>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @GET("users/{userID}")
    fun getUserProfileSingle(
            @Header("x-api-token") token: String,
            @Path("userID") userID: Int): Flowable<JsonObject>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @FormUrlEncoded
    @POST("account/my-profile/email")
    fun changeEmail(@Header("x-api-token") token: String,
                    @Field("email") userEmail: String,
                    @Field("current_password") currentPassword: String): Flowable<JsonObject>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @FormUrlEncoded
    @POST("account/my-profile/password")
    fun changePassword(@Header("x-api-token") token: String,
                       @Field("plainPassword[first]") newPassword: String,
                       @Field("plainPassword[second]") newPasswordRepeat: String,
                       @Field("current_password") plainPassword: String): Flowable<JsonObject>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @Multipart
    @POST("account/my-profile")
    fun editProfile(@Header("x-api-token") token: String,
                    @Part("firstName") firstName: RequestBody,
                    @Part("lastName") lastName: RequestBody,
                    @Part image: MultipartBody.Part,
                    @Part("description") description: RequestBody): Flowable<JsonObject>


    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @FormUrlEncoded
    @POST("users/{userID}/message")
    fun messageUser(@Header("x-api-token") token: String,
                    @Path("userID") userID: Int,
                    @Field("body") body: String): Flowable<JsonObject>

    //EVENTS
    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @GET("events")
    fun getEventsList(@QueryMap options: Map<String, String>? = null): Flowable<EventsData>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @GET("events/{eventID}")
    fun getEvent(@Path("eventID") eventID: Int): Flowable<Event>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @Multipart
    @POST("events")
    fun createEvent(@Header("x-api-token") token: String,
                    @PartMap eventValues: HashMap<String, RequestBody>,
                    @Part image: MultipartBody.Part): Flowable<EventsData>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @Multipart
    @POST("events/{eventID}")
    fun editEvent(@Header("x-api-token") token: String,
                  @Path("eventID") eventID: Int,
                  @PartMap eventValues: HashMap<String, RequestBody>,
                  @Part image: MultipartBody.Part): Flowable<EventsData>

    //CLUBS
    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @GET("clubs")
    fun getClubsList(@QueryMap options: Map<String, String>? = null): Flowable<ClubsData>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @GET("clubs/{clubID}")
    fun getClub(@Header("x-api-token") token: String? = null,
                @Path("clubID") clubID: Int): Flowable<Club>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @GET("clubs/mine")
    fun getClubsMine(@Header("x-api-token") token: String,
                     @QueryMap options: Map<String, String>? = null): Flowable<ClubsData>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @Multipart
    @POST("clubs")
    fun createClub(@Header("x-api-token") token: String,
                   @PartMap clubValues: HashMap<String, RequestBody>,
                   @Part image: MultipartBody.Part): Flowable<JsonObject>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @DELETE("clubs/{clubID}")
    fun deleteClub(@Header("x-api-token") token: String,
                   @Path("clubID") clubID: Int): Flowable<JsonObject>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @Multipart
    @POST("clubs/{clubID}")
    fun editClub(@Header("x-api-token") token: String,
                 @Path("clubID") clubID: Int,
                 @PartMap clubValues: HashMap<String, RequestBody>,
                 @Part image: MultipartBody.Part): Flowable<JsonObject>

    //Messages & Comments
    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @GET("conversations")
    fun getMessageList(@Header("x-api-token") token: String): Flowable<MessageData>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @GET("conversations/{conversationID}/messages")
    fun getChatMessages(@Header("x-api-token") token: String,
                        @Path("conversationID") conversationID: Int): Flowable<ChatData>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @FormUrlEncoded
    @POST("conversations/{conversationID}/messages")
    fun postMessage(@Header("x-api-token") token: String,
                    @Path("conversationID") conversationID: Int,
                    @Field("body") message: String): Flowable<JsonObject>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @GET("clubs/{clubID}/comments")
    fun getComments(@Header("x-api-token") token: String,
                    @Path("clubID") clubId: Int): Flowable<CommentsData>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @FormUrlEncoded
    @POST("clubs/{clubID}/comments")
    fun postComment(@Header("x-api-token") token: String,
                    @Path("clubID") clubId: Int,
                    @Field("body") message: String): Flowable<JsonObject>

    //Awards
    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @GET("users/{userID}/awards")
    fun getAwards(@Header("x-api-token") token: String,
                  @Path("userID") userID: Int): Flowable<AwardsData>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @Multipart
    @POST("users/account/awards")
    fun createAward(@Header("x-api-token") token: String,
                    @PartMap awardValues: HashMap<String, String>): Flowable<JsonObject>

    //Media
    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @GET("clubs/{clubID}/media")
    fun getClubMedia(@Header("x-api-token") token: String,
                     @Path("clubID") clubID: Int): Flowable<MediaData>

    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @Multipart
    @POST("clubs/{clubID}/media")
    fun postMedia(@Header("x-api-token") token: String,
                  @Path("clubID") clubID: Int,
                  @Part image: MultipartBody.Part): Flowable<JsonObject>

    //Notifications
    @Headers("x-api-key: 1084391c65a6ad0e368bc10a56b5f721c0266ef9ae106ba873ebdb999e718f8a")
    @GET("notifications")
    fun getNotifications(@Header("x-api-token") token: String): Flowable<NotificationsData>
}